
{
    "name": "rmX1",
    "id": "138a8c0e-200e-4cd1-9e63-ecf87fb420c5",
    "creationCodeFile": "",
    "inheritCode": true,
    "inheritCreationOrder": true,
    "inheritLayers": true,
    "instanceCreationOrderIDs": [
        "8d89cb95-0c98-457a-b840-c122123e8e95",
        "501ab697-e23c-47f2-92ac-372dc5bca2bc",
        "5928e769-e905-4c6b-b3c6-1511c6f267dc",
        "aa741738-a463-446a-85bf-8ecf1649234a",
        "1f918453-d223-494d-abf0-399f17e19c7c",
        "dd5156e4-2485-470b-9b1b-4b898431b625",
        "b017b225-0784-4210-a877-22831b5ec798",
        "32921645-432e-4d29-b062-5c7b924f2515",
        "44ab68b6-4008-4cc1-9206-5826df1aaf97",
        "e8490533-70c0-4838-b03f-de995432b487",
        "53b430c5-c5d6-4861-81bd-e9e4109eace2",
        "e0b09f3c-1e65-4d14-a543-287dc5e02b76",
        "213922dc-50cd-4aac-9552-37e95592b471",
        "84408492-44c8-4b53-b800-b655c8b547ae",
        "4f8a2a07-983c-4ca3-8adc-5d5e44f69c12",
        "75c062cb-3045-4163-aba8-809d235df288",
        "76c27390-4390-4761-aa7a-c49ce7afff75",
        "e8b573d3-0eae-4076-880a-b6ffa04e45c8",
        "9b95d6cd-5746-4565-bd1c-85f08b843aa9",
        "06d65e8e-03fe-45f5-8905-f7d4ec7d477c",
        "48498730-8207-410a-a68f-521894c13616",
        "42e4971e-3004-45c0-a573-d9d268a5c7aa",
        "a64b41a5-4ad6-472e-9b68-b60f52dcb2ae",
        "52b53fe5-991b-4946-84c9-94529852474c",
        "54422850-9963-47ad-b30a-e09f80685ace",
        "99717509-7e48-4d72-aaad-4585f6360a67",
        "7ff49d84-8957-4fd6-a1e2-93782c3c72b8",
        "8abd31a0-63bc-491b-b1ad-5fef24f6db59",
        "43f81f80-9063-407e-a1f9-c68327b92084",
        "b394ab80-922c-43bf-85e0-93a0fc25736b",
        "54bae281-e752-40a1-8e49-9c16383a3c02",
        "f6f320e9-593a-4019-b406-184729d3d2ff",
        "c3d36278-c435-4317-850d-5047338cde73",
        "b15ecc7f-3237-4ede-97b1-9473317f335f",
        "42b94e23-2fac-4078-920a-298cfa44ed4d",
        "ff7e7c7b-f998-4cc3-a5e1-f55fcc14f6c4",
        "fcda3570-dd6e-4f46-8eb4-6a448db6fac0",
        "d37ba11f-7b83-44a3-8aec-788014145dd3",
        "255eba3a-e20f-4d82-b609-41018c7ed626",
        "3e0c472e-ef15-432e-aea4-468f56e6860d",
        "b3a5e313-7d8e-4b48-ae48-d036454f78fd",
        "97aa8d44-8b10-4b35-b9bb-ec8207551691",
        "fb5ed0c5-4a81-4f49-9a68-5629e3b19c38"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Grid",
            "id": "51b56b36-6b53-4463-846d-7a79e9981781",
            "animationFPS": 10,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 0,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [

            ],
            "m_parentID": "eaf2cbbb-525a-4b7c-abc7-cf9ced794d15",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "47eb71b4-16a3-4c64-b03b-fe4ec685ca86",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Arrows",
            "id": "f356d447-0a85-48a9-a016-f34f893215b5",
            "depth": 100,
            "grid_x": 8,
            "grid_y": 8,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "1621a950-b894-405b-8817-b35998767d71",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "ebbeb54a-2e24-4250-95e9-fdbb524a3f02",
            "depth": 200,
            "grid_x": 8,
            "grid_y": 8,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "instances": [
{"name": "inst_4DD27BE9","id": "8d89cb95-0c98-457a-b840-c122123e8e95","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4DD27BE9","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 128,"y": 0},
{"name": "inst_6E5DDAC8","id": "501ab697-e23c-47f2-92ac-372dc5bca2bc","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6E5DDAC8","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 176,"y": 0},
{"name": "inst_8B3CF5F","id": "5928e769-e905-4c6b-b3c6-1511c6f267dc","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_8B3CF5F","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 128,"y": 144},
{"name": "inst_320CBDE5","id": "aa741738-a463-446a-85bf-8ecf1649234a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_320CBDE5","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 112,"y": 128},
{"name": "inst_20A7F999","id": "1f918453-d223-494d-abf0-399f17e19c7c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_20A7F999","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 96,"y": 112},
{"name": "inst_238C0237","id": "dd5156e4-2485-470b-9b1b-4b898431b625","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_238C0237","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 112,"y": 96},
{"name": "inst_319E824B","id": "b017b225-0784-4210-a877-22831b5ec798","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_319E824B","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 112,"y": 16},
{"name": "inst_53E9CF3B","id": "32921645-432e-4d29-b062-5c7b924f2515","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_53E9CF3B","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 96,"y": 32},
{"name": "inst_64BABF30","id": "44ab68b6-4008-4cc1-9206-5826df1aaf97","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_64BABF30","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 112,"y": 48},
{"name": "inst_45C2B094","id": "e8490533-70c0-4838-b03f-de995432b487","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_45C2B094","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 192,"y": 16},
{"name": "inst_49F8A5E2","id": "53b430c5-c5d6-4861-81bd-e9e4109eace2","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_49F8A5E2","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 208,"y": 32},
{"name": "inst_51A2E1D6","id": "e0b09f3c-1e65-4d14-a543-287dc5e02b76","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_51A2E1D6","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 192,"y": 48},
{"name": "inst_115BF498","id": "213922dc-50cd-4aac-9552-37e95592b471","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_115BF498","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 192,"y": 96},
{"name": "inst_1E212155","id": "84408492-44c8-4b53-b800-b655c8b547ae","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1E212155","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 208,"y": 112},
{"name": "inst_4F6E7DDB","id": "4f8a2a07-983c-4ca3-8adc-5d5e44f69c12","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4F6E7DDB","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 192,"y": 128},
{"name": "inst_3B0BCA9F","id": "75c062cb-3045-4163-aba8-809d235df288","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3B0BCA9F","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 176,"y": 144},
{"name": "inst_5A61BA67","id": "76c27390-4390-4761-aa7a-c49ce7afff75","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5A61BA67","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 192,"y": 0},
{"name": "inst_6F40A8F7","id": "e8b573d3-0eae-4076-880a-b6ffa04e45c8","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6F40A8F7","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 208,"y": 0},
{"name": "inst_3E9BCC50","id": "9b95d6cd-5746-4565-bd1c-85f08b843aa9","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3E9BCC50","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 208,"y": 16},
{"name": "inst_4CDCBE1A","id": "06d65e8e-03fe-45f5-8905-f7d4ec7d477c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4CDCBE1A","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 192,"y": 64},
{"name": "inst_6F22CAF6","id": "48498730-8207-410a-a68f-521894c13616","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6F22CAF6","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 192,"y": 80},
{"name": "inst_72B4A44F","id": "42e4971e-3004-45c0-a573-d9d268a5c7aa","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_72B4A44F","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 208,"y": 48},
{"name": "inst_52AEAB81","id": "a64b41a5-4ad6-472e-9b68-b60f52dcb2ae","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_52AEAB81","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 208,"y": 64},
{"name": "inst_56D135B3","id": "52b53fe5-991b-4946-84c9-94529852474c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_56D135B3","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 208,"y": 80},
{"name": "inst_50161D1A","id": "54422850-9963-47ad-b30a-e09f80685ace","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_50161D1A","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 208,"y": 96},
{"name": "inst_1DEB0518","id": "99717509-7e48-4d72-aaad-4585f6360a67","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1DEB0518","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 208,"y": 128},
{"name": "inst_18D57D0F","id": "7ff49d84-8957-4fd6-a1e2-93782c3c72b8","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_18D57D0F","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 208,"y": 144},
{"name": "inst_7CDC57AA","id": "8abd31a0-63bc-491b-b1ad-5fef24f6db59","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7CDC57AA","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 192,"y": 144},
{"name": "inst_65E0DBA4","id": "43f81f80-9063-407e-a1f9-c68327b92084","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_65E0DBA4","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 6,"scaleY": 10,"mvc": "1.1","x": 224,"y": 0},
{"name": "inst_756C900","id": "b394ab80-922c-43bf-85e0-93a0fc25736b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_756C900","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 6,"scaleY": 10,"mvc": "1.1","x": 0,"y": 0},
{"name": "inst_232726A0","id": "54bae281-e752-40a1-8e49-9c16383a3c02","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_232726A0","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 96,"y": 128},
{"name": "inst_64E54F72","id": "f6f320e9-593a-4019-b406-184729d3d2ff","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_64E54F72","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 96,"y": 144},
{"name": "inst_79BE34DD","id": "c3d36278-c435-4317-850d-5047338cde73","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_79BE34DD","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 112,"y": 144},
{"name": "inst_F6B9E9D","id": "b15ecc7f-3237-4ede-97b1-9473317f335f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_F6B9E9D","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 96,"y": 96},
{"name": "inst_4C901BD7","id": "42b94e23-2fac-4078-920a-298cfa44ed4d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4C901BD7","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 96,"y": 80},
{"name": "inst_784A2065","id": "ff7e7c7b-f998-4cc3-a5e1-f55fcc14f6c4","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_784A2065","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 96,"y": 64},
{"name": "inst_15896DB6","id": "fcda3570-dd6e-4f46-8eb4-6a448db6fac0","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_15896DB6","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 96,"y": 48},
{"name": "inst_58F2EEE1","id": "d37ba11f-7b83-44a3-8aec-788014145dd3","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_58F2EEE1","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 112,"y": 64},
{"name": "inst_66FD59FF","id": "255eba3a-e20f-4d82-b609-41018c7ed626","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_66FD59FF","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 112,"y": 80},
{"name": "inst_1B200F94","id": "3e0c472e-ef15-432e-aea4-468f56e6860d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1B200F94","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 96,"y": 0},
{"name": "inst_4D851E9B","id": "b3a5e313-7d8e-4b48-ae48-d036454f78fd","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4D851E9B","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 96,"y": 16},
{"name": "inst_7004F52D","id": "97aa8d44-8b10-4b35-b9bb-ec8207551691","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7004F52D","objId": "a1a89055-2740-4f3f-a9c8-2ca8cb7e268c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 112,"y": 0},
{"name": "inst_783C0328","id": "fb5ed0c5-4a81-4f49-9a68-5629e3b19c38","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_783C0328","objId": "952ec08c-42f1-4f70-9e58-5a06b83a2155","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 160,"y": 80}
            ],
            "layers": [

            ],
            "m_parentID": "7d69f375-e80c-4d39-ac91-fc822d3d4439",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "b7ecd311-45ea-428e-bef0-70447c542d5f",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4278190080 },
            "depth": 300,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [

            ],
            "m_parentID": "f21db608-1210-4e62-9ee4-1986267705b6",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "aec47b92-13f1-462d-814f-a3c1b68d88de",
    "physicsSettings":     {
        "id": "45f3dfc5-b757-43c9-a0b5-58144253da66",
        "inheritPhysicsSettings": true,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "3fbb7557-5a6c-4003-a9f6-80ab989cd0ff",
        "Height": 180,
        "inheritRoomSettings": true,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 320
    },
    "mvc": "1.0",
    "views": [
{"id": "89056633-f432-48ce-ae3c-a4dcd6bc905e","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "3e9410e3-0adc-46fd-b209-7e73d70baa62","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "be662ab0-7d93-4d77-b95d-27d71350e868","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "875716eb-0865-4cb4-81fa-13ef5c3d093f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "d8057d6b-2cae-440b-a066-0cb2d80cb26f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5210c657-e621-4900-aad2-f0f267952a57","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5bf4f849-9c22-434b-a9d9-56b104f158c8","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "75f5b9c5-3243-4aa7-8330-1ba105a8717c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "eab7c18b-06ae-4ba6-9e27-34b5926798f6",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": true,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}