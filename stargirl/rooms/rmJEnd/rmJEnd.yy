
{
    "name": "rmJEnd",
    "id": "a5a3d9da-95c7-4c25-8ac0-0511f0b05854",
    "creationCodeFile": "",
    "inheritCode": true,
    "inheritCreationOrder": true,
    "inheritLayers": true,
    "instanceCreationOrderIDs": [
        "3fbdb0a8-41b7-46b6-bc92-9a15c908d2df"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Grid",
            "id": "d5986c40-42dd-44fc-9355-8598a42cf5b3",
            "animationFPS": 10,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 0,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": false,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "eaf2cbbb-525a-4b7c-abc7-cf9ced794d15",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "47eb71b4-16a3-4c64-b03b-fe4ec685ca86",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": false,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Arrows",
            "id": "ce0ad1f0-ad02-4e92-b69c-89f9929f32b1",
            "depth": 100,
            "grid_x": 8,
            "grid_y": 8,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "1621a950-b894-405b-8817-b35998767d71",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "80f7ec97-5a99-4acc-8c48-0e9eeb85b93a",
            "depth": 200,
            "grid_x": 8,
            "grid_y": 8,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "instances": [
{"name": "inst_793DA0B5","id": "3fbdb0a8-41b7-46b6-bc92-9a15c908d2df","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_793DA0B5.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_793DA0B5","objId": "8cc2a363-b7fe-4d69-8a3d-e0fd673f217b","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 160,"y": 24}
            ],
            "layers": [

            ],
            "m_parentID": "7d69f375-e80c-4d39-ac91-fc822d3d4439",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "21e148f2-bc56-4ea4-86da-b72386a5e96f",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4278190080 },
            "depth": 300,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [

            ],
            "m_parentID": "f21db608-1210-4e62-9ee4-1986267705b6",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "aec47b92-13f1-462d-814f-a3c1b68d88de",
    "physicsSettings":     {
        "id": "824b39ba-1bc6-4921-8750-8111b907a4aa",
        "inheritPhysicsSettings": true,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "3732b5ec-e7cd-4a41-91de-19e626486d27",
        "Height": 180,
        "inheritRoomSettings": true,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 320
    },
    "mvc": "1.0",
    "views": [
{"id": "800cc86b-a824-4c09-bd59-44316c0ad068","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "4be2379c-78b7-4658-a2ef-ed92d6c2e92a","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5ca1d895-14f0-4d32-a56d-ec17a1034ee3","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "4375e485-8fb8-4b5c-93d2-48894cf0006b","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "8a179d50-56d2-4e51-af43-45d85ee9dd2e","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "6d931882-b9b1-4d40-b748-15de1b73c8b5","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "bcf0d6e6-7d01-49ee-9f6c-a85c9c634547","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "a1755615-481a-4fed-90f4-ce22f04a6375","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "99ab806f-23e7-4c46-a04d-6f0eddd42041",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": true,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}