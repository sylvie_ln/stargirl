
{
    "name": "rmTEnd",
    "id": "8ff04cdb-8854-4388-8b5e-452bb95d5a5b",
    "creationCodeFile": "",
    "inheritCode": true,
    "inheritCreationOrder": true,
    "inheritLayers": true,
    "instanceCreationOrderIDs": [
        "eea3bb1e-3283-4812-99b2-83d21c39f48a"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Grid",
            "id": "469af59b-4e2d-4c10-b32d-794c0d5b276d",
            "animationFPS": 10,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 0,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": false,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "eaf2cbbb-525a-4b7c-abc7-cf9ced794d15",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "47eb71b4-16a3-4c64-b03b-fe4ec685ca86",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": false,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Arrows",
            "id": "9f08c997-b7d3-45f8-9fe9-cd962b87b83c",
            "depth": 100,
            "grid_x": 8,
            "grid_y": 8,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "1621a950-b894-405b-8817-b35998767d71",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "855ebe81-79d9-4d70-9067-2b9ea1aeb37b",
            "depth": 200,
            "grid_x": 8,
            "grid_y": 8,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "instances": [
{"name": "inst_7D62A436","id": "eea3bb1e-3283-4812-99b2-83d21c39f48a","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_7D62A436.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7D62A436","objId": "8cc2a363-b7fe-4d69-8a3d-e0fd673f217b","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 160,"y": 24}
            ],
            "layers": [

            ],
            "m_parentID": "7d69f375-e80c-4d39-ac91-fc822d3d4439",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "5784fcfc-4c49-4eb0-a325-ada5e0311470",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4278190080 },
            "depth": 300,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [

            ],
            "m_parentID": "f21db608-1210-4e62-9ee4-1986267705b6",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "aec47b92-13f1-462d-814f-a3c1b68d88de",
    "physicsSettings":     {
        "id": "102649e5-515d-46f3-92a7-517b38a5d440",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "c9ca00e1-ec8e-4cea-8c68-d1a96f948b0e",
        "Height": 180,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 320
    },
    "mvc": "1.0",
    "views": [
{"id": "ad185e42-982a-4c53-9bb7-965649d7c2d4","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "12fba2a3-2371-47e5-8352-9443b1b695b2","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "fb021802-63d2-45d6-a687-6ea48b7d2091","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "1c42616b-12a2-4631-b529-5d2ebe831573","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "6e2730de-e36f-473e-b064-7d8603b03fb1","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "6a933543-3744-4fd5-b45c-e2958365fc47","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b14ad8c2-9874-4795-9f94-f64828460fa6","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ca791c0a-3094-436c-9852-88e667e4c44b","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "d3a11b27-c53e-48bb-8c13-3658b6a5957a",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}