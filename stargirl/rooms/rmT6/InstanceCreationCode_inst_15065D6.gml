text = "Welcome to Heaven.\nHere all desires are granted,\nforever."
if global.hell {
	y -= 4;
	with oChar {
		y += 8;
	}
	text =@"So you know my secret.
That's right, I imprisoned 
Satan and took over Hell.
Time to die, fool who stares 
down God without fear.
Touch me to begin the battle."
with oCollect { instance_destroy(); }
}