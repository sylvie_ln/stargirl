{
    "id": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadFaceButtons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 67,
    "bbox_right": 85,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5dedbc05-683b-483d-9d6a-0c4bcb1eacb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "31e75b48-69f4-48e5-bc41-fe187c9bffc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dedbc05-683b-483d-9d6a-0c4bcb1eacb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "616103a1-45cd-4d18-a2c2-bde3d0e39600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dedbc05-683b-483d-9d6a-0c4bcb1eacb5",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "60111c41-4619-454a-9da3-663183e1f973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dedbc05-683b-483d-9d6a-0c4bcb1eacb5",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "ba69a258-02e8-4859-aad0-886fb1274407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "47d59baa-ef51-4ae4-96b4-bd8af8511c8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba69a258-02e8-4859-aad0-886fb1274407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e1d8b2f-5aba-42b5-9ea3-d7209d234a10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba69a258-02e8-4859-aad0-886fb1274407",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "0e183874-72bd-4d70-a271-4615a3358733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba69a258-02e8-4859-aad0-886fb1274407",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "22ed3254-dd17-442c-9c3b-969df9a54aaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "d11af06b-508b-4c6b-a94f-4806944917a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22ed3254-dd17-442c-9c3b-969df9a54aaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5f15f48-3db5-441d-9763-f2800b450fa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22ed3254-dd17-442c-9c3b-969df9a54aaa",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "b015d5d8-7af9-4d62-b630-479bfe672994",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22ed3254-dd17-442c-9c3b-969df9a54aaa",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "bf4147e0-1548-40bc-807f-fa09a9b48ca0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "cf24fca8-93a0-48de-93d4-8bb99be05076",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf4147e0-1548-40bc-807f-fa09a9b48ca0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcd57922-6276-41ae-845a-096e5e15840c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf4147e0-1548-40bc-807f-fa09a9b48ca0",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "5bd99482-9010-4f88-b8f4-878de157bda5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf4147e0-1548-40bc-807f-fa09a9b48ca0",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "7e0d4e7e-b5aa-4610-b97f-fa52b6018286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "fbebe5b9-4c9e-451f-95f1-8c09a42621bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e0d4e7e-b5aa-4610-b97f-fa52b6018286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0170499f-8a07-428c-852b-e20b958d682d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e0d4e7e-b5aa-4610-b97f-fa52b6018286",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "20a3bb25-25d4-41b3-a0aa-7dbb524f91d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e0d4e7e-b5aa-4610-b97f-fa52b6018286",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "3669c59a-af64-4ee4-9a41-ac63c91b3d77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "0ff1c2ad-c827-4146-88e4-3a59d0dc8a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3669c59a-af64-4ee4-9a41-ac63c91b3d77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3997ed7a-60a0-4105-abb4-9a19d5e7498b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3669c59a-af64-4ee4-9a41-ac63c91b3d77",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "3256000c-d4cb-426b-82b8-baebabeace45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3669c59a-af64-4ee4-9a41-ac63c91b3d77",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "303584e5-a9ed-4d36-a1d1-dadec2df0b40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "e51130c5-23f3-4046-8c6e-3c99ebc084f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "303584e5-a9ed-4d36-a1d1-dadec2df0b40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c20ba8bb-ebbb-4873-918b-a8d3abfac084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "303584e5-a9ed-4d36-a1d1-dadec2df0b40",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "c5526044-be98-43a6-8395-ee312f4a94fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "303584e5-a9ed-4d36-a1d1-dadec2df0b40",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "d521f8b1-2ae6-4c46-af10-1447e58c8222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "28d37509-f631-4245-9e44-c4e8a849982c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d521f8b1-2ae6-4c46-af10-1447e58c8222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68bf8e8b-3bab-499f-b83b-8ac0f6eb6d6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d521f8b1-2ae6-4c46-af10-1447e58c8222",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "17485189-212e-42e8-afb8-00884826758e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d521f8b1-2ae6-4c46-af10-1447e58c8222",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "c929b76b-3de7-476f-8e2d-9f926c9960b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "623b818e-65b9-4ce1-a0f3-80bdbd12ccbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c929b76b-3de7-476f-8e2d-9f926c9960b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42d179de-5b08-4c29-aace-cb03494566c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c929b76b-3de7-476f-8e2d-9f926c9960b7",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "8e1e19d6-6d62-4798-8ff8-250cf825e762",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c929b76b-3de7-476f-8e2d-9f926c9960b7",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "905e44e8-58d1-41c8-9f5f-dc8066293712",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "7f2548ea-fba9-4e1a-b48d-81e3a13014a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "905e44e8-58d1-41c8-9f5f-dc8066293712",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64bbf7f1-228b-4e6c-9c08-2ef7cd3e5fad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "905e44e8-58d1-41c8-9f5f-dc8066293712",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "d017d3c5-38c6-44b8-a7bd-5de4a662600b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "905e44e8-58d1-41c8-9f5f-dc8066293712",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "28678357-1035-4187-89d4-ea1bc986d0b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "87ca8088-6e6f-427a-b328-8f58517c4bbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28678357-1035-4187-89d4-ea1bc986d0b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e7e585d-14fc-4ccd-acfb-f0f93b19b567",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28678357-1035-4187-89d4-ea1bc986d0b2",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "ef14a139-3e7e-4915-bead-5fe0e5a91bbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28678357-1035-4187-89d4-ea1bc986d0b2",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "ea95d99d-975d-4d65-959f-d6b7cad76fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "7e53c345-a17e-441f-ac3e-0e80756fa131",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea95d99d-975d-4d65-959f-d6b7cad76fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab7db85c-37cf-4e7e-890c-40d95604092b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea95d99d-975d-4d65-959f-d6b7cad76fdb",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "4037e300-5f2d-4e63-b05f-a9b52bde18cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea95d99d-975d-4d65-959f-d6b7cad76fdb",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "9249166b-7a7d-4717-9de9-e51144e987dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "af1aecd4-cd70-49b2-8465-9e0e46eb5ef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9249166b-7a7d-4717-9de9-e51144e987dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7878e34e-595e-4ff6-8a0c-a3f9050e1101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9249166b-7a7d-4717-9de9-e51144e987dc",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "726fb02c-8114-4a8a-a7a8-a9bc2bac32c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9249166b-7a7d-4717-9de9-e51144e987dc",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "f881db36-aa69-4d48-996a-f3699fe5bcac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "c995b88c-830c-45ed-a0f4-dada43f9f372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f881db36-aa69-4d48-996a-f3699fe5bcac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd207803-1653-4078-a6fa-b46395b8bd94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f881db36-aa69-4d48-996a-f3699fe5bcac",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "f80297c4-adbb-45dc-b9d4-e5d2eaf1d508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f881db36-aa69-4d48-996a-f3699fe5bcac",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "08625ff2-0f7a-4777-9b1a-021890803c65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "a7490f90-c14a-49bb-87bd-ed0c332cff91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08625ff2-0f7a-4777-9b1a-021890803c65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc9926fe-5b68-452f-9d2c-da3bfd258df1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08625ff2-0f7a-4777-9b1a-021890803c65",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "75f5621a-d1cb-4c87-a5c7-4827f2780078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08625ff2-0f7a-4777-9b1a-021890803c65",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        },
        {
            "id": "88747971-afdd-4128-99f7-0046ebc9ef17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "compositeImage": {
                "id": "f2e8699e-69cb-433c-a2b1-6ce15c36ce2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88747971-afdd-4128-99f7-0046ebc9ef17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8924e592-0cbc-4fdf-a4e3-83ec2f54904b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88747971-afdd-4128-99f7-0046ebc9ef17",
                    "LayerId": "35f4d484-bcae-44a8-8642-0f3037dd2de6"
                },
                {
                    "id": "4375d237-e9e5-481b-9545-add4741078aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88747971-afdd-4128-99f7-0046ebc9ef17",
                    "LayerId": "d13ceec4-40b7-4668-a55d-06f2c24738e8"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "35f4d484-bcae-44a8-8642-0f3037dd2de6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d13ceec4-40b7-4668-a55d-06f2c24738e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d5fcdc7-1ebf-41b9-93aa-32c396425482",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}