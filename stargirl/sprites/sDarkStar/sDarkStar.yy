{
    "id": "2405d297-2427-4ed4-b2a4-05cebfbaea19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDarkStar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9beeab90-e1ba-484e-afd0-07c369e235c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2405d297-2427-4ed4-b2a4-05cebfbaea19",
            "compositeImage": {
                "id": "7ef4d8da-9b4a-4460-b4d7-422b5fc68ac8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9beeab90-e1ba-484e-afd0-07c369e235c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2126d1a8-0aff-481f-9310-f123debe4d3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9beeab90-e1ba-484e-afd0-07c369e235c9",
                    "LayerId": "52e100e2-9dae-4f7b-9975-85e69e64b8a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "52e100e2-9dae-4f7b-9975-85e69e64b8a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2405d297-2427-4ed4-b2a4-05cebfbaea19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}