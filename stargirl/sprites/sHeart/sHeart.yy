{
    "id": "e93f7a5a-ab21-428b-a557-9699829a80bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d73dc80-c46b-4536-bdb6-1fcc6806690d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e93f7a5a-ab21-428b-a557-9699829a80bb",
            "compositeImage": {
                "id": "1a5e247a-b75d-41c3-bf39-dd7833f05ade",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d73dc80-c46b-4536-bdb6-1fcc6806690d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "858c579d-b683-43d4-a5ca-74749e8b7cf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d73dc80-c46b-4536-bdb6-1fcc6806690d",
                    "LayerId": "7f2721f9-0187-4f5a-9890-c54125540de8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7f2721f9-0187-4f5a-9890-c54125540de8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e93f7a5a-ab21-428b-a557-9699829a80bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}