{
    "id": "654f966b-a719-48ca-af15-2c69e9e93353",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNPC",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d365895a-2b19-4990-8223-25d897e011da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "654f966b-a719-48ca-af15-2c69e9e93353",
            "compositeImage": {
                "id": "fd6d3e81-4bc0-40e6-aaa9-06b35d54b33a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d365895a-2b19-4990-8223-25d897e011da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1a9fb49-0f29-4f8c-9535-22e23d7460f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d365895a-2b19-4990-8223-25d897e011da",
                    "LayerId": "60483d36-9553-4bef-83f7-d49db893181f"
                }
            ]
        },
        {
            "id": "6881c05c-0e00-4d7e-8bad-84911d75f30d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "654f966b-a719-48ca-af15-2c69e9e93353",
            "compositeImage": {
                "id": "88ef71e3-6bd6-4837-b956-f4a7198bd9f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6881c05c-0e00-4d7e-8bad-84911d75f30d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32d22291-09ef-4d8f-939c-ac07fbccb051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6881c05c-0e00-4d7e-8bad-84911d75f30d",
                    "LayerId": "60483d36-9553-4bef-83f7-d49db893181f"
                }
            ]
        },
        {
            "id": "9d299f8d-ba9d-4ba1-99e0-45010821b90a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "654f966b-a719-48ca-af15-2c69e9e93353",
            "compositeImage": {
                "id": "67df8db7-e099-4c6c-a427-46157c02aa45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d299f8d-ba9d-4ba1-99e0-45010821b90a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfc34c09-ab3f-4cd4-b889-c95c763aba63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d299f8d-ba9d-4ba1-99e0-45010821b90a",
                    "LayerId": "60483d36-9553-4bef-83f7-d49db893181f"
                }
            ]
        },
        {
            "id": "d554beb3-80bb-4652-ac46-940fa180b472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "654f966b-a719-48ca-af15-2c69e9e93353",
            "compositeImage": {
                "id": "ed813e84-7010-4482-a0bc-8a68c5ed12c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d554beb3-80bb-4652-ac46-940fa180b472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea70e81c-eaa7-4d55-a451-844c4bfe6131",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d554beb3-80bb-4652-ac46-940fa180b472",
                    "LayerId": "60483d36-9553-4bef-83f7-d49db893181f"
                }
            ]
        },
        {
            "id": "bdd396f1-ba0b-4f99-94c0-7d4de9135124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "654f966b-a719-48ca-af15-2c69e9e93353",
            "compositeImage": {
                "id": "fd6ab04d-0261-482a-bcca-72c538cb4701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdd396f1-ba0b-4f99-94c0-7d4de9135124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "728a2913-819e-4b7e-9ede-d5b7237da6bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdd396f1-ba0b-4f99-94c0-7d4de9135124",
                    "LayerId": "60483d36-9553-4bef-83f7-d49db893181f"
                }
            ]
        },
        {
            "id": "4f1f2499-e0bb-4b9e-a85e-2953e5a487a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "654f966b-a719-48ca-af15-2c69e9e93353",
            "compositeImage": {
                "id": "c75ea432-611c-40a9-82b6-017372cd3d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f1f2499-e0bb-4b9e-a85e-2953e5a487a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14bb10e4-3186-470c-b3be-babd69939fec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f1f2499-e0bb-4b9e-a85e-2953e5a487a4",
                    "LayerId": "60483d36-9553-4bef-83f7-d49db893181f"
                }
            ]
        },
        {
            "id": "c52674cd-1b95-4497-b7b8-e7eb32ae3072",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "654f966b-a719-48ca-af15-2c69e9e93353",
            "compositeImage": {
                "id": "b390f83b-6ff2-4c52-a6d6-727f67124354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c52674cd-1b95-4497-b7b8-e7eb32ae3072",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1100fb10-bf28-4884-8607-96b111dd7201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c52674cd-1b95-4497-b7b8-e7eb32ae3072",
                    "LayerId": "60483d36-9553-4bef-83f7-d49db893181f"
                }
            ]
        },
        {
            "id": "e4aafec0-9c85-45d7-bfb7-fe874a839e18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "654f966b-a719-48ca-af15-2c69e9e93353",
            "compositeImage": {
                "id": "eeef050d-3e16-410c-951b-2401fc14ff13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4aafec0-9c85-45d7-bfb7-fe874a839e18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ac0c3aa-22ee-4e78-9ec6-7cfeb672df98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4aafec0-9c85-45d7-bfb7-fe874a839e18",
                    "LayerId": "60483d36-9553-4bef-83f7-d49db893181f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "60483d36-9553-4bef-83f7-d49db893181f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "654f966b-a719-48ca-af15-2c69e9e93353",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}