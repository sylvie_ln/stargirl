{
    "id": "e5fed572-873d-4c79-9988-11c7da9c93b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyAction",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e6b05e9-e649-49b9-9af4-5254dfda60cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5fed572-873d-4c79-9988-11c7da9c93b7",
            "compositeImage": {
                "id": "cb72b4ea-958d-481a-a80e-49258c4a8183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e6b05e9-e649-49b9-9af4-5254dfda60cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1eb998f-d8ce-4ce2-805e-8587d93a0c76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e6b05e9-e649-49b9-9af4-5254dfda60cc",
                    "LayerId": "fc8fbdde-a409-4202-87db-17d8f26a2b3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "fc8fbdde-a409-4202-87db-17d8f26a2b3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5fed572-873d-4c79-9988-11c7da9c93b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}