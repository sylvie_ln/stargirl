{
    "id": "dd2d3089-6c3d-4a00-b823-8fc882874b96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLeftArrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84e9c866-d4f5-424a-8c79-d3e614cac969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd2d3089-6c3d-4a00-b823-8fc882874b96",
            "compositeImage": {
                "id": "fad26c10-29e6-418c-916c-feec0a1289fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84e9c866-d4f5-424a-8c79-d3e614cac969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5830017c-84ca-4f8c-b76c-5dec850c325a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84e9c866-d4f5-424a-8c79-d3e614cac969",
                    "LayerId": "39d679e9-2a09-41f0-966d-655d5b3dc55c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "39d679e9-2a09-41f0-966d-655d5b3dc55c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd2d3089-6c3d-4a00-b823-8fc882874b96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}