{
    "id": "83277d49-251e-446f-bec3-39bca5aeffd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93aea9e2-27ac-435e-a5e7-e6c04f06d342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83277d49-251e-446f-bec3-39bca5aeffd1",
            "compositeImage": {
                "id": "673bf710-3f63-48f7-842e-eb03d62f6b63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93aea9e2-27ac-435e-a5e7-e6c04f06d342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4970e83-bce2-48dd-9f56-9b25262400ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93aea9e2-27ac-435e-a5e7-e6c04f06d342",
                    "LayerId": "7d3840d4-13ad-4acf-8806-e22fd67df5c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7d3840d4-13ad-4acf-8806-e22fd67df5c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83277d49-251e-446f-bec3-39bca5aeffd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}