{
    "id": "cd5f267c-c6bc-499f-857b-baf0c122ba6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLargeGem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adcc6119-8ed4-4cf4-b251-ff351696aaf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd5f267c-c6bc-499f-857b-baf0c122ba6c",
            "compositeImage": {
                "id": "b92f519d-c8b5-40b3-9fc1-aabf9c4172aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adcc6119-8ed4-4cf4-b251-ff351696aaf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb135655-b5a1-4d85-b9d8-be3b79c7d103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adcc6119-8ed4-4cf4-b251-ff351696aaf1",
                    "LayerId": "a3a79045-14b8-4fd6-8d1a-6e90db8ba60b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a3a79045-14b8-4fd6-8d1a-6e90db8ba60b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd5f267c-c6bc-499f-857b-baf0c122ba6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}