{
    "id": "ba69e9f5-3c9e-4505-ad78-f17dd3540d80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGirlSU",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "464e5338-ed77-4ffe-9b0a-c44df88dbd8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba69e9f5-3c9e-4505-ad78-f17dd3540d80",
            "compositeImage": {
                "id": "7ded9e5e-781e-43e7-88e3-ba48cdc73ecf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "464e5338-ed77-4ffe-9b0a-c44df88dbd8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a68d6ed-4a19-44a8-8641-49572c5b1f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "464e5338-ed77-4ffe-9b0a-c44df88dbd8b",
                    "LayerId": "d02b09ea-e136-475f-977d-df2b693a7a1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d02b09ea-e136-475f-977d-df2b693a7a1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba69e9f5-3c9e-4505-ad78-f17dd3540d80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        553648127,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4280173158,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}