{
    "id": "1631c0c9-6bb9-49d4-b6c9-a84fc2ae3026",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23eeda4b-5dce-4fba-9ddb-6f23fb9020a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1631c0c9-6bb9-49d4-b6c9-a84fc2ae3026",
            "compositeImage": {
                "id": "898fcc70-af42-4cf3-a9e7-97af46a97e33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23eeda4b-5dce-4fba-9ddb-6f23fb9020a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde9da32-96ee-40c1-9c37-f49321f0cd0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23eeda4b-5dce-4fba-9ddb-6f23fb9020a2",
                    "LayerId": "4207c241-9ca4-49b0-b774-0bc4a08ca20a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4207c241-9ca4-49b0-b774-0bc4a08ca20a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1631c0c9-6bb9-49d4-b6c9-a84fc2ae3026",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}