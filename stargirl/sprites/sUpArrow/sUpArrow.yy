{
    "id": "bbea60b9-1afe-4e81-951f-2ad00fc4b430",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sUpArrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5936259d-ecb4-4ea1-af15-6bc396172bd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbea60b9-1afe-4e81-951f-2ad00fc4b430",
            "compositeImage": {
                "id": "5342fe11-dc6c-4fad-93cf-5238afe9d723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5936259d-ecb4-4ea1-af15-6bc396172bd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e18d1d3-6469-476d-9823-e665e7bcc1f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5936259d-ecb4-4ea1-af15-6bc396172bd6",
                    "LayerId": "5feb919b-7ba8-4b47-9bdd-0f9f94b4f3ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "5feb919b-7ba8-4b47-9bdd-0f9f94b4f3ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbea60b9-1afe-4e81-951f-2ad00fc4b430",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}