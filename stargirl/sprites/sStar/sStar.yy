{
    "id": "fd02b5f9-3445-47aa-9a36-73e1fe513346",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe38d622-8b80-485e-b32c-c719a69e715b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd02b5f9-3445-47aa-9a36-73e1fe513346",
            "compositeImage": {
                "id": "aa55f339-e79b-4d26-a441-b53513009672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe38d622-8b80-485e-b32c-c719a69e715b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21bd8619-7a11-431e-9de6-c6deb5b85f6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe38d622-8b80-485e-b32c-c719a69e715b",
                    "LayerId": "f9925966-89c4-405b-b6af-9d3d534ce677"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f9925966-89c4-405b-b6af-9d3d534ce677",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd02b5f9-3445-47aa-9a36-73e1fe513346",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}