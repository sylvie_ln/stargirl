{
    "id": "cda3617a-4681-466f-9159-bf6f77e59147",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDownArrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92fc4932-63e3-4e69-90c1-2a2a145cfe00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cda3617a-4681-466f-9159-bf6f77e59147",
            "compositeImage": {
                "id": "87ae13a5-929f-4ca9-9a12-b804140df13b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92fc4932-63e3-4e69-90c1-2a2a145cfe00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b36c90e-4927-49e9-9080-a626df92a8c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92fc4932-63e3-4e69-90c1-2a2a145cfe00",
                    "LayerId": "86755c02-5de2-46f9-8c87-5b1f8c208d1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "86755c02-5de2-46f9-8c87-5b1f8c208d1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cda3617a-4681-466f-9159-bf6f77e59147",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}