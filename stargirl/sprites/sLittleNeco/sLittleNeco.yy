{
    "id": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLittleNeco",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eba160d5-3ea5-4d53-b2bc-0ede9c29ae48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "7d614937-9179-4094-9828-3f6ea87251c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eba160d5-3ea5-4d53-b2bc-0ede9c29ae48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5e78119-e0e2-4f5e-8f55-111652ba06bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eba160d5-3ea5-4d53-b2bc-0ede9c29ae48",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "8cd9df71-c117-492c-a629-80350b6ee3dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "9cd5faca-60b0-4d7c-b30e-5cbc6c2f4e74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd9df71-c117-492c-a629-80350b6ee3dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f528fa2-b53d-4725-b362-ea442040af70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd9df71-c117-492c-a629-80350b6ee3dd",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "39e80ef2-39bd-4abe-a5ff-5b366d602268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "82d2d41e-b4e2-49f5-90d2-1a607c7716fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39e80ef2-39bd-4abe-a5ff-5b366d602268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "130a05b0-e181-4aa0-9f97-81a422b9ede2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39e80ef2-39bd-4abe-a5ff-5b366d602268",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "56333e65-5abf-4717-ac92-961c94aa8870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "f3a86266-32d8-484c-9019-e4b4b35ec1e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56333e65-5abf-4717-ac92-961c94aa8870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12336e34-363f-454d-b71b-579dfad31851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56333e65-5abf-4717-ac92-961c94aa8870",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "d481db8b-c0be-4861-beeb-c32e36a3f1bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "8785d805-9278-4c9e-94b9-d3f4480cdfe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d481db8b-c0be-4861-beeb-c32e36a3f1bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "951ca2a4-3c10-46dd-8b14-bc531fa4c095",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d481db8b-c0be-4861-beeb-c32e36a3f1bd",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "a1668033-9c28-4fa8-ba71-3c5eee322b63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "a5e7bdf4-4fea-495a-82f8-4a87ba2efc69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1668033-9c28-4fa8-ba71-3c5eee322b63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f55ef559-0da8-4757-b575-c42212fb1591",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1668033-9c28-4fa8-ba71-3c5eee322b63",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "14da650d-2cf8-4b8c-b4c4-dbd5b5bf2231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "605df331-7f43-4647-9c05-f7a4c330f445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14da650d-2cf8-4b8c-b4c4-dbd5b5bf2231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc342851-b808-4bd1-9dfa-937bcf18019d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14da650d-2cf8-4b8c-b4c4-dbd5b5bf2231",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "4b47c770-9564-48a4-9d93-cadd44618471",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "339d450a-724d-4e3f-a848-23d31c5a09cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b47c770-9564-48a4-9d93-cadd44618471",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c7bd912-d397-4fb6-bdd5-52c658dec1d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b47c770-9564-48a4-9d93-cadd44618471",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "423af168-db80-454c-951a-a92928aa0807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "99e67090-d092-42ad-a067-c31ca1047001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "423af168-db80-454c-951a-a92928aa0807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ab8589-e673-40be-9794-67903dd7e20d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "423af168-db80-454c-951a-a92928aa0807",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "8ec1ae7f-e5fd-4ff4-a9a4-5f2832343fad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "1be10a23-4eb3-4e0d-9fa3-e47836f71484",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ec1ae7f-e5fd-4ff4-a9a4-5f2832343fad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbe46bb5-50f0-410b-a8a3-64e834a1dc9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ec1ae7f-e5fd-4ff4-a9a4-5f2832343fad",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "00ecad3b-338c-4b22-b4f3-c634aeba5a3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "9432e77b-a9c4-47a3-ad4f-ee7cf14d8493",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00ecad3b-338c-4b22-b4f3-c634aeba5a3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6af287a-fd9c-48b0-82d4-9b4470aa3bd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00ecad3b-338c-4b22-b4f3-c634aeba5a3b",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "24b54540-1c87-4e4f-8387-28883554a7cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "e3588542-bd6d-42cd-92a4-8f4baf6f0bc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24b54540-1c87-4e4f-8387-28883554a7cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0627d54-8efc-4f8f-a7ec-0ad8adc2c976",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24b54540-1c87-4e4f-8387-28883554a7cd",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "7267b26b-0f88-429f-b458-8182641d636f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "b747f30a-d62c-4c92-ba15-8dec95b5a08c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7267b26b-0f88-429f-b458-8182641d636f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dca7651c-640b-43dc-8c71-c5163fb4619b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7267b26b-0f88-429f-b458-8182641d636f",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "6809eff5-4730-4dd5-a129-e89cdd3d7bd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "6b289d70-d051-4596-ad78-b8a4cf3034bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6809eff5-4730-4dd5-a129-e89cdd3d7bd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6827060d-a170-4ee9-9731-cd943b01b951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6809eff5-4730-4dd5-a129-e89cdd3d7bd2",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "c65145b4-37dd-4f95-b1a2-11d25060024a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "949eb985-1b35-4189-9ce2-f3afbe58c703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c65145b4-37dd-4f95-b1a2-11d25060024a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bdd1349-e47a-444c-9f67-fbc0e991566b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c65145b4-37dd-4f95-b1a2-11d25060024a",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "9133d4fa-b63b-425a-b9bb-8e5e99f8b0b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "96489db8-0785-4fd6-af51-c0b883651bfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9133d4fa-b63b-425a-b9bb-8e5e99f8b0b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca43daa7-aa96-4148-9584-7ed05a8280ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9133d4fa-b63b-425a-b9bb-8e5e99f8b0b4",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "4bd24eff-083d-485c-89de-d01ef47b7d15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "ba67cd42-4abb-4657-b4da-5b389a024931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bd24eff-083d-485c-89de-d01ef47b7d15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "080f4532-6ce0-4924-9329-010ae19d8986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd24eff-083d-485c-89de-d01ef47b7d15",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "deb58f75-4ee5-4992-9b2e-22b564defa40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "233ae4d6-8acf-4bce-afb2-1f7eff0346a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deb58f75-4ee5-4992-9b2e-22b564defa40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c134d3b-a19f-4521-92c0-0889a9162761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deb58f75-4ee5-4992-9b2e-22b564defa40",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "cb8d595b-9282-4d7b-8d02-44ac8d994d4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "e4dbfe09-5a0f-4b2a-9fe9-f7e9f2313b84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb8d595b-9282-4d7b-8d02-44ac8d994d4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad6bd283-7a8b-41bb-b025-547fc27f8a59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb8d595b-9282-4d7b-8d02-44ac8d994d4c",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "4e08961e-36fb-4025-a853-be2a8e095f87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "082c9355-cd87-4cd3-b08f-60e14c793670",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e08961e-36fb-4025-a853-be2a8e095f87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8d90e47-13d6-43cb-a9c3-cb754a7c28f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e08961e-36fb-4025-a853-be2a8e095f87",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "26b59c6b-742b-4e31-ab26-abcd8e202ba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "80219cde-fecb-44de-983e-507e305252de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26b59c6b-742b-4e31-ab26-abcd8e202ba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feb273a6-9d21-4b42-b2a4-4b6967cbfd59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26b59c6b-742b-4e31-ab26-abcd8e202ba5",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "aa70892b-feec-42d5-aed5-822f977f56b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "c9c87caa-6727-48d1-ab2e-cba3f5e33f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa70892b-feec-42d5-aed5-822f977f56b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53e49ca5-44cd-414b-8d68-704a9a027e32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa70892b-feec-42d5-aed5-822f977f56b4",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "d68579b4-a112-4bcd-8d6d-1f349c012a46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "9053f92f-3d68-4f28-9ca5-8708340954c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d68579b4-a112-4bcd-8d6d-1f349c012a46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c065cc3-171f-40cc-89a7-55e8581ccb61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d68579b4-a112-4bcd-8d6d-1f349c012a46",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "65ec0551-b239-4ee0-8ce1-6c016aa2a751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "8769464c-50fb-4c60-9394-185566090125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65ec0551-b239-4ee0-8ce1-6c016aa2a751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd123c35-10ae-46fc-9607-cee3a5d7cae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65ec0551-b239-4ee0-8ce1-6c016aa2a751",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "9fe6b7e6-1f01-4916-b271-1e0076eed9dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "2e914e77-7598-48f6-9b09-f08348e660ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fe6b7e6-1f01-4916-b271-1e0076eed9dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e05a1b56-2372-4506-9e9c-a9ea767ce242",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fe6b7e6-1f01-4916-b271-1e0076eed9dc",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "e14972de-3320-4b53-b5f1-5d9749bdd27d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "2aeb2d20-f0c4-4470-958b-557cf0d02e19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e14972de-3320-4b53-b5f1-5d9749bdd27d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8463d532-81a2-4f98-a783-02623f05a7b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e14972de-3320-4b53-b5f1-5d9749bdd27d",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "6e96f513-9807-4bb7-b0aa-021ec680ea64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "f00717ac-769a-420f-bf0e-29cef73c03f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e96f513-9807-4bb7-b0aa-021ec680ea64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46dcf6e1-32ca-4500-a0a8-585c3ad4ae57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e96f513-9807-4bb7-b0aa-021ec680ea64",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "f0c9c77e-aefe-4cd9-baa0-94145b152d14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "056737f2-217b-4d85-aa83-01d344f08356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c9c77e-aefe-4cd9-baa0-94145b152d14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d969b871-e922-4a7e-8b12-66bb3461b193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c9c77e-aefe-4cd9-baa0-94145b152d14",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "6979b9bc-7c8c-4dfd-96b5-0f6541c9870e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "d9143fcb-eb2d-4de7-9b51-fb799e0d7140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6979b9bc-7c8c-4dfd-96b5-0f6541c9870e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5261cde7-7ee4-454d-86eb-d5ad112ac858",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6979b9bc-7c8c-4dfd-96b5-0f6541c9870e",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "c91407d6-e786-4f8f-b2c5-bff56cb1b4b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "fc02a827-c346-4426-b575-51829235db91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c91407d6-e786-4f8f-b2c5-bff56cb1b4b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6083020-7607-48ef-8cb5-7d6a579f40fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c91407d6-e786-4f8f-b2c5-bff56cb1b4b6",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "4fbdc918-5b5b-4357-bb9d-37946fd3ec61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "b9915b31-e3d7-47f8-b3fe-f8d4a6cfd785",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fbdc918-5b5b-4357-bb9d-37946fd3ec61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f6ce7a6-dc1f-4d8c-a471-ebda320b3fb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fbdc918-5b5b-4357-bb9d-37946fd3ec61",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "58f2dc68-360e-4100-8cb9-360469da72c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "3663a6be-04c4-44e0-a216-a05ef6f1e4c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58f2dc68-360e-4100-8cb9-360469da72c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b336560d-8aca-43f9-a415-75dee325dc84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58f2dc68-360e-4100-8cb9-360469da72c7",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "b97e8b97-a8ed-4d1b-8bf2-ffcd3284cdb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "a88671e8-3919-475f-a521-a1f25a461738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b97e8b97-a8ed-4d1b-8bf2-ffcd3284cdb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af74c157-9f6a-43f7-b384-979f69ddb49f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b97e8b97-a8ed-4d1b-8bf2-ffcd3284cdb5",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "517b3601-5861-42d4-8584-c89924dced61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "ffab508f-309e-4087-acb8-dd56be7dc8ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "517b3601-5861-42d4-8584-c89924dced61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a78947c1-f1a8-4c81-875c-9a1035b92cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "517b3601-5861-42d4-8584-c89924dced61",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "112ed068-2ace-4d61-8583-e26ae7c2c01f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "6a201720-20de-4e89-a155-d893899b6d53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "112ed068-2ace-4d61-8583-e26ae7c2c01f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5937dc-0789-4bfa-aed9-9705b27cd1f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "112ed068-2ace-4d61-8583-e26ae7c2c01f",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "4b06c2d7-49e9-44ba-89b3-8a77732e5db5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "5639f442-28f6-4f7e-911d-169c02a71db2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b06c2d7-49e9-44ba-89b3-8a77732e5db5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3e4bef3-265d-407c-b267-36353d3b0266",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b06c2d7-49e9-44ba-89b3-8a77732e5db5",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "63759f12-c6bd-4a8c-a285-b11e9690e4c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "8bb02b96-ca72-4643-8b55-640fd02e472d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63759f12-c6bd-4a8c-a285-b11e9690e4c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0855ff86-d58a-4e57-bf8d-bdf1627718bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63759f12-c6bd-4a8c-a285-b11e9690e4c8",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "c909dc48-4ee8-4824-8c5e-7875f7c78cce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "8d6625aa-8074-4c15-9554-40b9c7a38772",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c909dc48-4ee8-4824-8c5e-7875f7c78cce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1fa4f8f-9e82-4b24-ae59-d785a9dc2a10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c909dc48-4ee8-4824-8c5e-7875f7c78cce",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "ee075a35-4b74-4d73-8cd9-43fe8003a745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "82096835-4254-4961-b984-e74c66d6befb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee075a35-4b74-4d73-8cd9-43fe8003a745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "710795de-03e6-48fd-ab36-39df78033375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee075a35-4b74-4d73-8cd9-43fe8003a745",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "b9d92f13-c154-4c36-96d7-9d86aa4a453e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "c850ccac-67af-4775-b745-c1c61dd848a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d92f13-c154-4c36-96d7-9d86aa4a453e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7169b5ed-978a-4db7-b49f-d76f61c1cdc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d92f13-c154-4c36-96d7-9d86aa4a453e",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "94f6fa74-7223-4152-ba3b-adf987931e53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "99affe52-4548-45bd-85bb-2439297fd92b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94f6fa74-7223-4152-ba3b-adf987931e53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7a7676a-75d5-47c8-8ea1-f845421b53ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94f6fa74-7223-4152-ba3b-adf987931e53",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "fdb3dafb-e1bb-4e30-b4a9-e672e95c691c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "3aa29902-3614-4c59-b08c-4bf2a3054a9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdb3dafb-e1bb-4e30-b4a9-e672e95c691c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c872d5e9-e183-4593-86dc-e6318d69e7d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdb3dafb-e1bb-4e30-b4a9-e672e95c691c",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "1ea81630-72cf-43ae-8066-59a23b803e8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "fac42d2b-8904-421e-89fb-1b00d19642c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ea81630-72cf-43ae-8066-59a23b803e8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad01c376-b634-4b6a-a02c-5740948b3df8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ea81630-72cf-43ae-8066-59a23b803e8a",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "aa570445-aeb3-4c56-ac12-dd9b4cced792",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "054d79fa-2657-407d-8a14-648404926057",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa570445-aeb3-4c56-ac12-dd9b4cced792",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30661792-535b-4515-a3a7-c7ebe120e15b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa570445-aeb3-4c56-ac12-dd9b4cced792",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "69a14e21-ed16-4f27-8397-f973e25f3f47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "e0332957-70f4-4e1e-8f02-f4b20c1d3f26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69a14e21-ed16-4f27-8397-f973e25f3f47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d4bb076-fd5c-4de8-a710-786e1ae4e08c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69a14e21-ed16-4f27-8397-f973e25f3f47",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "a8348b7e-5122-4820-8c98-02edf45be983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "ee6ba391-e095-4ceb-9893-f6e552b5b36b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8348b7e-5122-4820-8c98-02edf45be983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16801cb9-2e07-450d-87b2-31be1295f8b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8348b7e-5122-4820-8c98-02edf45be983",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "3b3ae0ca-3dfa-49ec-993f-565c8e9976ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "ffda85be-0939-40e3-bb79-4482020b6f1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b3ae0ca-3dfa-49ec-993f-565c8e9976ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1900e5b3-78df-4846-91e2-70cf1596d4ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b3ae0ca-3dfa-49ec-993f-565c8e9976ee",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "899f6a2b-af05-48e1-b422-b63d62afb220",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "2faec05b-f773-49fd-89e1-2cb6ee0c06a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "899f6a2b-af05-48e1-b422-b63d62afb220",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "234c70ca-2a13-4ad6-a8b6-ab77eaaff43f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "899f6a2b-af05-48e1-b422-b63d62afb220",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "0454699d-7068-4365-b7e0-7e3e7133b817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "a193419b-f59f-40f1-a4b5-9a4787616aa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0454699d-7068-4365-b7e0-7e3e7133b817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "284852a4-a6b0-49e5-8e90-2a925faae162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0454699d-7068-4365-b7e0-7e3e7133b817",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "b47624f3-e438-4cd9-8d95-64d3d1930fac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "c888b138-83fc-4217-9e6a-a8a39552c5da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b47624f3-e438-4cd9-8d95-64d3d1930fac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff1d12f-17ee-4a94-b67b-c6be6525d3ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b47624f3-e438-4cd9-8d95-64d3d1930fac",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "fc1af0ab-350d-4f68-b99f-91a32d9ce152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "4a3d7ef2-5665-4538-9c6f-c387c5591383",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc1af0ab-350d-4f68-b99f-91a32d9ce152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "275fecd1-a15e-438e-bdc3-397c41861284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc1af0ab-350d-4f68-b99f-91a32d9ce152",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "e0655691-344b-48d5-a56f-d02331927b84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "89f3d441-144c-4791-8ee9-a514f6307b18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0655691-344b-48d5-a56f-d02331927b84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0175bba-809d-4abe-8d21-622da4b20538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0655691-344b-48d5-a56f-d02331927b84",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "ab87128c-24d2-4162-b127-aa52bb1ede5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "d7da4ca0-ea6e-4979-ab87-212e2acd3ce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab87128c-24d2-4162-b127-aa52bb1ede5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b9dfb5b-d42b-43b4-b35c-306da4e176d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab87128c-24d2-4162-b127-aa52bb1ede5a",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "0f6eb6dd-0bbd-487b-840e-45d7e2619d0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "37b5d2f1-f260-4e96-b7d9-7bc43e9aaa39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f6eb6dd-0bbd-487b-840e-45d7e2619d0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "067a3dd7-d035-44dc-86be-83c55bba2123",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f6eb6dd-0bbd-487b-840e-45d7e2619d0e",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "9bc5fcec-9aad-4682-975a-6de2d5124c19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "c965edad-c7f7-4fb9-8fbe-b92eb18f1cec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bc5fcec-9aad-4682-975a-6de2d5124c19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5321e2ad-cbf8-4b8b-8200-3ad468d964e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bc5fcec-9aad-4682-975a-6de2d5124c19",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "a16566a6-3e21-4131-a9b7-c9a26e6b1d86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "c39a242b-c70f-4a89-8b22-9a79c6774c09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a16566a6-3e21-4131-a9b7-c9a26e6b1d86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d77ac604-0b20-492e-b789-0565ee8fd806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a16566a6-3e21-4131-a9b7-c9a26e6b1d86",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "f7ad7c9f-b727-43a0-960e-8d68b9af50ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "7dad5ad6-1d12-4337-bf31-7b7cc719eef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ad7c9f-b727-43a0-960e-8d68b9af50ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b3c764c-0ba6-41c6-971a-2e3b8dc5f8bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ad7c9f-b727-43a0-960e-8d68b9af50ab",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "42a0708d-16d2-484a-83c7-6ddfc24ba5c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "94a50a11-09b1-4452-a510-c333b9177d12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42a0708d-16d2-484a-83c7-6ddfc24ba5c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb4bddd1-aa59-4b61-a95e-7a1902c1b4b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42a0708d-16d2-484a-83c7-6ddfc24ba5c8",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "8a3c4d56-b8d5-4a7b-879a-a7e6cb95f358",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "69910b99-4d60-4992-84f1-a4d7c6d6f8d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a3c4d56-b8d5-4a7b-879a-a7e6cb95f358",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17730103-d2a6-45fd-9d58-70da5be40e12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a3c4d56-b8d5-4a7b-879a-a7e6cb95f358",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "f69cc38a-f193-4bd2-8d07-dc177e05febb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "d1c991de-6145-4cb2-9114-4bef56060d4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f69cc38a-f193-4bd2-8d07-dc177e05febb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be72d3f0-7db7-4ce7-ac2d-e3c3db888d34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f69cc38a-f193-4bd2-8d07-dc177e05febb",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "daf504e0-c9d3-4b18-b07f-8a42adccf074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "e6901caa-9c33-41ab-875a-c383dac1ba37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daf504e0-c9d3-4b18-b07f-8a42adccf074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b44f80-512f-40fa-a183-2121a61ad8b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daf504e0-c9d3-4b18-b07f-8a42adccf074",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "292856c3-c4b7-4600-8f3f-20299a23c792",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "e52ed176-c962-4e6a-ba2f-0c4b7fb40b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "292856c3-c4b7-4600-8f3f-20299a23c792",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a689142b-038c-4588-94db-b744e03e470e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "292856c3-c4b7-4600-8f3f-20299a23c792",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "652e3940-e641-4f1c-a5b6-98bbe0a68c92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "4bb7cd15-d5c8-4fa6-b928-5a165aa4c1cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "652e3940-e641-4f1c-a5b6-98bbe0a68c92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74aedb9c-2b72-4bf1-a1d9-21280b7110d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "652e3940-e641-4f1c-a5b6-98bbe0a68c92",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "c298cb52-a0d7-45e0-9fdf-10099fd5df36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "bfc438c7-bf86-4a2f-bd10-0e01fb297511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c298cb52-a0d7-45e0-9fdf-10099fd5df36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fea7410-458f-41b0-924c-e9b2cb6700c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c298cb52-a0d7-45e0-9fdf-10099fd5df36",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "bc3ba649-22d4-48be-bd54-a655cdf34a4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "85210a8b-e4d4-4468-9959-b85326fafda1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc3ba649-22d4-48be-bd54-a655cdf34a4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db2de10f-2bb2-41b1-9d0e-85ae6d38bb64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc3ba649-22d4-48be-bd54-a655cdf34a4d",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "f96fc76e-838d-4867-a6b4-756b97f7a18e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "0bb23502-b948-488b-a5fe-d8a0a40663df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f96fc76e-838d-4867-a6b4-756b97f7a18e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be53e146-ea3d-4498-b07b-4ebc14a16108",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f96fc76e-838d-4867-a6b4-756b97f7a18e",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "b331fb81-8bfb-49b1-88df-ba47f0998060",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "acdac817-5a1f-4720-b859-875f0d329e8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b331fb81-8bfb-49b1-88df-ba47f0998060",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c649555c-0159-4e28-9b95-a0d7d2c3548f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b331fb81-8bfb-49b1-88df-ba47f0998060",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "53ee3df9-fb37-4e1e-bd0c-7a5d8a6e4d8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "2393a319-8269-431a-98e6-02d808c3a086",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53ee3df9-fb37-4e1e-bd0c-7a5d8a6e4d8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd500d6f-b8aa-4ba6-bef4-53a973170584",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53ee3df9-fb37-4e1e-bd0c-7a5d8a6e4d8c",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "76fde1c3-3314-4852-b584-bf72e001e907",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "54c1f762-42e4-446d-9151-3896651fef75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76fde1c3-3314-4852-b584-bf72e001e907",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90fe6829-d547-4c4e-b454-cc7330284caf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76fde1c3-3314-4852-b584-bf72e001e907",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "948b8e9d-91cc-4974-9df3-e33ded759da3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "366e488e-2101-4b07-a594-7aa7db7a75c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "948b8e9d-91cc-4974-9df3-e33ded759da3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a68af7b5-be14-4fd8-9428-9313acacdf83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "948b8e9d-91cc-4974-9df3-e33ded759da3",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "4d7b6c35-705e-4d18-93ba-58a39edd10ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "21ba75e5-6111-4b37-92a2-0490f86ade23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d7b6c35-705e-4d18-93ba-58a39edd10ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d79f697d-a872-4f88-b7fd-64cd0f55ebc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d7b6c35-705e-4d18-93ba-58a39edd10ab",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "6ad3ed41-64eb-4376-b2b2-b6d6d706a956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "0d744e42-6a20-4ab8-95f6-47e1417f8baa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ad3ed41-64eb-4376-b2b2-b6d6d706a956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dce9add0-ce78-4128-b613-50a5b008d2c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ad3ed41-64eb-4376-b2b2-b6d6d706a956",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "5ee26bde-37f1-40b8-bdb3-2e9bbcef3e5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "296845e5-f5dc-4190-ba9b-588310a7b2f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee26bde-37f1-40b8-bdb3-2e9bbcef3e5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b7cd3ef-5d15-4a16-a469-f8aa5990dd16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee26bde-37f1-40b8-bdb3-2e9bbcef3e5e",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "bb2dad42-d3e8-4472-bf5b-07d9ac86ea85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "29ee747a-8d36-4925-b7c7-9778d6ee4a9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb2dad42-d3e8-4472-bf5b-07d9ac86ea85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20c098e1-258f-4e5d-ab03-c47a0b88dd77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb2dad42-d3e8-4472-bf5b-07d9ac86ea85",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "e84c6601-cf9a-40a2-915e-700e32a4ac65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "d8b149b4-3776-49b8-ac68-32e1fbcc7609",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84c6601-cf9a-40a2-915e-700e32a4ac65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b8d10b5-1b63-4083-aa86-20e71f6466c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84c6601-cf9a-40a2-915e-700e32a4ac65",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "f6f5c421-46a1-42a9-9761-a34738c0e4cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "8f25633c-ffbc-4a65-a24c-b938ada40af4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6f5c421-46a1-42a9-9761-a34738c0e4cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfdedbca-be5f-46ed-b5ad-36419320ab04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6f5c421-46a1-42a9-9761-a34738c0e4cd",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "7adae550-0fcb-4adc-9a37-048d6f362c5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "49bd1ee7-39e1-48ca-b517-bc085613244b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7adae550-0fcb-4adc-9a37-048d6f362c5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f436c7e-70c8-47f6-878d-2f0ff5d6ef95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7adae550-0fcb-4adc-9a37-048d6f362c5b",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "05343ecd-12ae-49f5-97b3-9252cb8e6a85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "0bca225a-2fc4-4b80-953e-ec4bbe130ab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05343ecd-12ae-49f5-97b3-9252cb8e6a85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0588fdd-e030-47e6-9868-dda51ca4ed3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05343ecd-12ae-49f5-97b3-9252cb8e6a85",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "c59644eb-a5a0-4ea6-a965-eb05b0c61385",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "f89ef4e8-dcbd-4ae4-9d9e-b21d342d8deb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c59644eb-a5a0-4ea6-a965-eb05b0c61385",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f53ae56-a9f2-4206-b203-39f5ed6a7797",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c59644eb-a5a0-4ea6-a965-eb05b0c61385",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "142d0894-2419-42cc-b199-c4e5d011fd1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "6f64a824-2e14-4e5e-9ab4-24b14db3a2da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "142d0894-2419-42cc-b199-c4e5d011fd1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91fb01b2-5aaf-4d40-a676-e7a795a4b7f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "142d0894-2419-42cc-b199-c4e5d011fd1a",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "6ddadf6d-b41a-4f9e-bf53-694674970df4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "efe80250-3a3d-4615-8051-cd4b214c5956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ddadf6d-b41a-4f9e-bf53-694674970df4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32e17dc5-8792-4328-993b-ba7a6f4382ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ddadf6d-b41a-4f9e-bf53-694674970df4",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "9d610b19-56d5-4a56-8d0d-f24db58ff1f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "f90db804-999c-42e9-a2c8-09c420c9cddf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d610b19-56d5-4a56-8d0d-f24db58ff1f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb2e3ace-b8b5-49cd-934d-b5559bf4bc26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d610b19-56d5-4a56-8d0d-f24db58ff1f9",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "2c166ec1-9bf4-4881-8e08-e63a701ec26b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "a30c4a34-5cd9-4cf0-aa78-46c582bfeef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c166ec1-9bf4-4881-8e08-e63a701ec26b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "029e8d1f-b3d8-47f1-89ad-7039289207ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c166ec1-9bf4-4881-8e08-e63a701ec26b",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "f418a873-10b6-4185-af7b-8d846b0ca2d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "0d7cbb44-0059-465b-b086-3f58ec5c159f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f418a873-10b6-4185-af7b-8d846b0ca2d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1979e19-bc9b-48ac-b466-c2e8328d2a36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f418a873-10b6-4185-af7b-8d846b0ca2d7",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "17986b76-7ab9-4a82-950c-830186255aa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "3b9db9fa-abb9-4a54-a1c2-3024e31c30aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17986b76-7ab9-4a82-950c-830186255aa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54adab1f-0d88-44a8-9bcf-078a7b6a9280",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17986b76-7ab9-4a82-950c-830186255aa5",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "f19cc207-9832-4e50-9dc7-4221d3cd781e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "6cddcdf6-2cc8-4bd0-ba9b-87e219fc51de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f19cc207-9832-4e50-9dc7-4221d3cd781e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48ca23ad-f371-4d69-b7de-bc3431255235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f19cc207-9832-4e50-9dc7-4221d3cd781e",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "70e52d0b-94b8-43c6-a3ea-3f30cbae611b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "b2074fdd-c706-40c0-8cf4-41f8ef18dde2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70e52d0b-94b8-43c6-a3ea-3f30cbae611b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7db9e754-8af1-4b50-8a33-f7f411725049",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70e52d0b-94b8-43c6-a3ea-3f30cbae611b",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "a30f25df-95f4-4f42-8433-135dbdfa7519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "6369227e-68d6-4672-a94c-b207d16364c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a30f25df-95f4-4f42-8433-135dbdfa7519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9f42c3d-6b3b-460f-b030-4b1593c71a3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a30f25df-95f4-4f42-8433-135dbdfa7519",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "8d62c54b-0a18-46a3-8c5f-347694fe8e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "2af25e6b-d3a0-4688-a356-3b6b949c7f06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d62c54b-0a18-46a3-8c5f-347694fe8e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dc4a188-a621-4d14-8a75-4d5c49cc5d0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d62c54b-0a18-46a3-8c5f-347694fe8e5c",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "7bde2920-1354-4f5d-b610-d90cce2aeafd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "d15499a5-7ef6-4e95-9e01-4835d7b9b7d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bde2920-1354-4f5d-b610-d90cce2aeafd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72c0f909-76a5-4b1c-91ba-a32f66555951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bde2920-1354-4f5d-b610-d90cce2aeafd",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "fda49be4-f45c-4b78-b235-37f096551b0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "e762e382-6be8-47f8-a576-65a6fa8bf2a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fda49be4-f45c-4b78-b235-37f096551b0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4729a068-a626-4f3d-b3f4-014785c8094e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fda49be4-f45c-4b78-b235-37f096551b0b",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "224c9ba3-a8fd-4bdd-9177-70f7363c715a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "54c9a58f-0cf0-4346-a45c-fa3fe2a857f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "224c9ba3-a8fd-4bdd-9177-70f7363c715a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdd40049-7419-4120-9bd4-df3eba4088c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "224c9ba3-a8fd-4bdd-9177-70f7363c715a",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "0dd21282-be16-414a-9158-f33d5d841417",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "c9937d41-ca47-4fb0-a8eb-1daaca04a22a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dd21282-be16-414a-9158-f33d5d841417",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0857b454-6a02-411f-ad68-0ae8d863620d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dd21282-be16-414a-9158-f33d5d841417",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "5ef7c812-904d-46f1-b042-1b3bad29312c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "80ceb745-53d3-4788-8413-5d4ecdd90f58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ef7c812-904d-46f1-b042-1b3bad29312c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "984ba593-f0fa-49f0-a890-bf515076626e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ef7c812-904d-46f1-b042-1b3bad29312c",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "3b54dd0f-df22-4ff9-860d-8da5eaee5bfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "ea4aea11-bc7b-4789-84b4-d48b04386c1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b54dd0f-df22-4ff9-860d-8da5eaee5bfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29c4c510-0c8f-4334-a16c-fff859f9324b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b54dd0f-df22-4ff9-860d-8da5eaee5bfe",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        },
        {
            "id": "933f73b4-2037-4496-9819-4e104b5b05f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "compositeImage": {
                "id": "a7a5084c-5c64-470c-a3da-7d88ed13e3cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "933f73b4-2037-4496-9819-4e104b5b05f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c06bb3d-0487-4d2a-8041-229dd05c545a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "933f73b4-2037-4496-9819-4e104b5b05f8",
                    "LayerId": "4bac0303-af2a-44e4-a997-820a21d2330a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4bac0303-af2a-44e4-a997-820a21d2330a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e1128e6-c5d4-4219-ba0f-f8d9cc49591b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 0,
    "yorig": 0
}