{
    "id": "82d0a538-e9cb-46cf-bc95-625737bf829f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 32,
    "bbox_right": 39,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9989484e-2249-461b-bf6b-c78ec59fbcc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d0a538-e9cb-46cf-bc95-625737bf829f",
            "compositeImage": {
                "id": "8638393a-e0ac-4c49-840a-c6c773210e99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9989484e-2249-461b-bf6b-c78ec59fbcc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0e35dbe-0e6e-49ef-a790-76bca24905d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9989484e-2249-461b-bf6b-c78ec59fbcc1",
                    "LayerId": "c59ddbcc-4835-4317-9c7c-ea7ad066c8c6"
                },
                {
                    "id": "7a73a572-60c8-4228-bbb8-5604a7d27e84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9989484e-2249-461b-bf6b-c78ec59fbcc1",
                    "LayerId": "a5d1389f-31bb-4035-934a-9d7c08aea95c"
                }
            ]
        },
        {
            "id": "4ae99730-958f-449d-9047-a6cb7cce4afb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d0a538-e9cb-46cf-bc95-625737bf829f",
            "compositeImage": {
                "id": "d0704273-bc03-4fcb-960d-d7bbeda68f6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ae99730-958f-449d-9047-a6cb7cce4afb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38b59843-21ff-4fab-8727-46c77c3d7a2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ae99730-958f-449d-9047-a6cb7cce4afb",
                    "LayerId": "c59ddbcc-4835-4317-9c7c-ea7ad066c8c6"
                },
                {
                    "id": "9a7b49dc-67cf-4715-90d5-0bdaadc42a5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ae99730-958f-449d-9047-a6cb7cce4afb",
                    "LayerId": "a5d1389f-31bb-4035-934a-9d7c08aea95c"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "c59ddbcc-4835-4317-9c7c-ea7ad066c8c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82d0a538-e9cb-46cf-bc95-625737bf829f",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a5d1389f-31bb-4035-934a-9d7c08aea95c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82d0a538-e9cb-46cf-bc95-625737bf829f",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}