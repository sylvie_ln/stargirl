{
    "id": "2ca4657b-6830-4b63-b5ad-ddbbcb3dc140",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDarkStarSmall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7333d84-de07-4b5d-bd02-f8cbc19e52d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ca4657b-6830-4b63-b5ad-ddbbcb3dc140",
            "compositeImage": {
                "id": "348c00f6-c436-48b8-a22b-20986c5a1ba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7333d84-de07-4b5d-bd02-f8cbc19e52d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ceb8754-86d4-441d-9e44-495be516b5ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7333d84-de07-4b5d-bd02-f8cbc19e52d9",
                    "LayerId": "f502ad8b-2269-444f-a07d-3fb3faccbf13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "f502ad8b-2269-444f-a07d-3fb3faccbf13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ca4657b-6830-4b63-b5ad-ddbbcb3dc140",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}