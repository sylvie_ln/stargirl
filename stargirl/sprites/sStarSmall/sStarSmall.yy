{
    "id": "776ad9ae-06d8-48a6-9127-352e486a0921",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStarSmall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15bc1142-949a-4c74-844a-c8f2bb0bbde2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "776ad9ae-06d8-48a6-9127-352e486a0921",
            "compositeImage": {
                "id": "bda1bb8f-44eb-41c4-a162-915575348b62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15bc1142-949a-4c74-844a-c8f2bb0bbde2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95c24532-348f-4bc3-a580-fb46e6f573c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15bc1142-949a-4c74-844a-c8f2bb0bbde2",
                    "LayerId": "3c7e9b93-fcda-46f1-9bf0-94f56af601b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "3c7e9b93-fcda-46f1-9bf0-94f56af601b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "776ad9ae-06d8-48a6-9127-352e486a0921",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}