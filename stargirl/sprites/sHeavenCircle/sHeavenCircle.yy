{
    "id": "0d492430-4b9d-4ebb-bb78-55d7fc337b82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeavenCircle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 1,
    "bbox_right": 38,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "166c2d4d-86df-48cd-96cd-4469906f0cb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d492430-4b9d-4ebb-bb78-55d7fc337b82",
            "compositeImage": {
                "id": "21d2efd4-a037-4dfa-b05c-785e19f01c70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "166c2d4d-86df-48cd-96cd-4469906f0cb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a2e096c-180c-47f7-bba0-edd4659622f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "166c2d4d-86df-48cd-96cd-4469906f0cb2",
                    "LayerId": "a5816855-1f6d-4040-bd51-04465529923d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "a5816855-1f6d-4040-bd51-04465529923d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d492430-4b9d-4ebb-bb78-55d7fc337b82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 25,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}