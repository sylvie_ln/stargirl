{
    "id": "2a681887-967e-451e-99b4-c3807f470044",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDarkHole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "542a14bd-7358-47c5-a35f-6898b7731977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a681887-967e-451e-99b4-c3807f470044",
            "compositeImage": {
                "id": "4402b8a1-8bb9-4771-a754-86cd0788610c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "542a14bd-7358-47c5-a35f-6898b7731977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cbcc3bc-b50f-4086-91ed-261a5b9b4977",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "542a14bd-7358-47c5-a35f-6898b7731977",
                    "LayerId": "d668a7e7-b0de-494f-966b-fb297e52bc68"
                }
            ]
        },
        {
            "id": "adf5a57d-9cc5-412f-8b5a-b16af2710888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a681887-967e-451e-99b4-c3807f470044",
            "compositeImage": {
                "id": "a77932a8-39b9-4883-bd91-38df67adad18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adf5a57d-9cc5-412f-8b5a-b16af2710888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c2e912f-766e-4731-b3c2-40c543eb7f29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adf5a57d-9cc5-412f-8b5a-b16af2710888",
                    "LayerId": "d668a7e7-b0de-494f-966b-fb297e52bc68"
                }
            ]
        },
        {
            "id": "a947fc50-bd80-4785-8505-fcbdfb6e8985",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a681887-967e-451e-99b4-c3807f470044",
            "compositeImage": {
                "id": "da724e8b-bac6-4b81-b3e7-6fcf73ecbfaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a947fc50-bd80-4785-8505-fcbdfb6e8985",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d000b181-ef53-4a32-9988-22fcd3355ddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a947fc50-bd80-4785-8505-fcbdfb6e8985",
                    "LayerId": "d668a7e7-b0de-494f-966b-fb297e52bc68"
                }
            ]
        },
        {
            "id": "6be56149-7c1e-4372-8cf2-49ce1d896a97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a681887-967e-451e-99b4-c3807f470044",
            "compositeImage": {
                "id": "8ec63bfc-54bb-4ed5-9f3c-b9a2fea343ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6be56149-7c1e-4372-8cf2-49ce1d896a97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e8ee79d-185c-4160-8373-b6c5503ad70a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6be56149-7c1e-4372-8cf2-49ce1d896a97",
                    "LayerId": "d668a7e7-b0de-494f-966b-fb297e52bc68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d668a7e7-b0de-494f-966b-fb297e52bc68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a681887-967e-451e-99b4-c3807f470044",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}