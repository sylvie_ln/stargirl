{
    "id": "b5bf1f67-34c4-46f0-b818-b27b75518de3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHellCircle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 1,
    "bbox_right": 38,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16d1b425-729d-45be-a8bf-af02c8eded54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5bf1f67-34c4-46f0-b818-b27b75518de3",
            "compositeImage": {
                "id": "cf6dd248-751d-4600-a7db-eec5b2dbbfa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16d1b425-729d-45be-a8bf-af02c8eded54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40fe25a-a8d8-4bb0-bd63-79f1bb42593f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16d1b425-729d-45be-a8bf-af02c8eded54",
                    "LayerId": "2b5b32b6-1937-464f-993b-658d13991937"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "2b5b32b6-1937-464f-993b-658d13991937",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5bf1f67-34c4-46f0-b818-b27b75518de3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 25,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}