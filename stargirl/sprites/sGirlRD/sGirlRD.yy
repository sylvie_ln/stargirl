{
    "id": "8a2c5398-4886-450b-96bf-34dd13ec2ca1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGirlRD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81aba75a-77e2-4184-b6d2-8a505d932770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a2c5398-4886-450b-96bf-34dd13ec2ca1",
            "compositeImage": {
                "id": "af44a37c-7de7-49ea-875d-b9eb86d0775f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81aba75a-77e2-4184-b6d2-8a505d932770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff58afec-7298-4dfd-a3e4-65c60836bb4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81aba75a-77e2-4184-b6d2-8a505d932770",
                    "LayerId": "5d387c61-c923-4c50-8271-c2641195ea89"
                }
            ]
        },
        {
            "id": "03504fba-eda7-4a24-960a-9878a59ceb5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a2c5398-4886-450b-96bf-34dd13ec2ca1",
            "compositeImage": {
                "id": "6d1f0134-4b70-43ab-bb1e-5c5bf19dde5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03504fba-eda7-4a24-960a-9878a59ceb5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adaffead-fc77-4bc4-bad4-8703b4e3cb71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03504fba-eda7-4a24-960a-9878a59ceb5d",
                    "LayerId": "5d387c61-c923-4c50-8271-c2641195ea89"
                }
            ]
        },
        {
            "id": "b628a05e-a9c0-42b5-94d6-c38b851428fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a2c5398-4886-450b-96bf-34dd13ec2ca1",
            "compositeImage": {
                "id": "86a8e724-dd2d-4cb5-b301-7daf337922ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b628a05e-a9c0-42b5-94d6-c38b851428fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d66b836-d5e3-4f05-85dc-3ad7e39760ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b628a05e-a9c0-42b5-94d6-c38b851428fb",
                    "LayerId": "5d387c61-c923-4c50-8271-c2641195ea89"
                }
            ]
        },
        {
            "id": "424830cb-5bb3-49af-a082-e35078da1425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a2c5398-4886-450b-96bf-34dd13ec2ca1",
            "compositeImage": {
                "id": "0057b2ea-37ce-4662-a833-caa5bf88499d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "424830cb-5bb3-49af-a082-e35078da1425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c26686e-a74e-482b-a533-6512dde8f94a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "424830cb-5bb3-49af-a082-e35078da1425",
                    "LayerId": "5d387c61-c923-4c50-8271-c2641195ea89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5d387c61-c923-4c50-8271-c2641195ea89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a2c5398-4886-450b-96bf-34dd13ec2ca1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        553648127,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4280173158,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}