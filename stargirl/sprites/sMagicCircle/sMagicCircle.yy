{
    "id": "66c4834c-498d-4ff1-b35c-a5ebff3ec0be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMagicCircle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 1,
    "bbox_right": 38,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44f6cc1c-f5a4-4fab-959a-536d42bc985e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66c4834c-498d-4ff1-b35c-a5ebff3ec0be",
            "compositeImage": {
                "id": "fa798a4c-a127-4eb2-8d44-f954ea8ef5c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44f6cc1c-f5a4-4fab-959a-536d42bc985e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89713108-d507-488e-9ae9-3ce27a865721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44f6cc1c-f5a4-4fab-959a-536d42bc985e",
                    "LayerId": "7a4c749f-5a05-4d44-a782-5c2ea73635f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "7a4c749f-5a05-4d44-a782-5c2ea73635f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66c4834c-498d-4ff1-b35c-a5ebff3ec0be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 25,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}