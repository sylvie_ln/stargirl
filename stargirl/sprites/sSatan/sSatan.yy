{
    "id": "b46315b1-87d5-443d-9e3e-88ef1995d8a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSatan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d48a4e88-59f5-40ab-8869-43b931ad1067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b46315b1-87d5-443d-9e3e-88ef1995d8a8",
            "compositeImage": {
                "id": "1af91bc8-a79a-46f9-8352-20b6b2df07e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d48a4e88-59f5-40ab-8869-43b931ad1067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36b8af8a-c8e9-40bf-90f4-7cfa3ae4f154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d48a4e88-59f5-40ab-8869-43b931ad1067",
                    "LayerId": "1a35eb47-b1c8-496c-be6b-80645aca5256"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1a35eb47-b1c8-496c-be6b-80645aca5256",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b46315b1-87d5-443d-9e3e-88ef1995d8a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        553648127,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4279834905,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4280173158,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}