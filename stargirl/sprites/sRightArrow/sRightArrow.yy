{
    "id": "baac9249-1aa7-4b67-9474-bccdccc889c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRightArrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "573ec2a6-96dd-47ef-9b19-8a3165572540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baac9249-1aa7-4b67-9474-bccdccc889c8",
            "compositeImage": {
                "id": "51b22317-0907-41b8-88b3-10e0901a3880",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "573ec2a6-96dd-47ef-9b19-8a3165572540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cca99457-e2a7-48db-a670-c03e19344b80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573ec2a6-96dd-47ef-9b19-8a3165572540",
                    "LayerId": "29e3b8ab-b3aa-4454-8f65-db9758b577fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "29e3b8ab-b3aa-4454-8f65-db9758b577fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "baac9249-1aa7-4b67-9474-bccdccc889c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}