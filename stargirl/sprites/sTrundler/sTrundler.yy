{
    "id": "fbc0165d-8d48-48af-b44b-8e61eefbb94d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrundler",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2f3b824-be93-434f-bd2a-a44c2cc56539",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc0165d-8d48-48af-b44b-8e61eefbb94d",
            "compositeImage": {
                "id": "f87fd8f2-bd4e-4647-aa1c-a4225bf2bb34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2f3b824-be93-434f-bd2a-a44c2cc56539",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8692955e-8ab7-4d55-91c9-3af58ca1c6b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2f3b824-be93-434f-bd2a-a44c2cc56539",
                    "LayerId": "882c6c2c-00e6-49f4-a045-2756905f01f3"
                }
            ]
        },
        {
            "id": "973e0852-9e3e-483f-83b5-1b23ea953a26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc0165d-8d48-48af-b44b-8e61eefbb94d",
            "compositeImage": {
                "id": "dcf5ab6c-a234-4615-96b8-5d61d96cd9ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "973e0852-9e3e-483f-83b5-1b23ea953a26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "794cb972-3079-4119-aacd-4bcf4915996d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "973e0852-9e3e-483f-83b5-1b23ea953a26",
                    "LayerId": "882c6c2c-00e6-49f4-a045-2756905f01f3"
                }
            ]
        },
        {
            "id": "3d293f47-7c1f-4ad2-8bc5-f2bce9c3a729",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc0165d-8d48-48af-b44b-8e61eefbb94d",
            "compositeImage": {
                "id": "9cb7a204-38fc-4c9e-b88c-00ab39267041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d293f47-7c1f-4ad2-8bc5-f2bce9c3a729",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17a34ee6-451f-463b-805c-3220a2c8bc74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d293f47-7c1f-4ad2-8bc5-f2bce9c3a729",
                    "LayerId": "882c6c2c-00e6-49f4-a045-2756905f01f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "882c6c2c-00e6-49f4-a045-2756905f01f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbc0165d-8d48-48af-b44b-8e61eefbb94d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}