{
    "id": "2685f5a9-d28d-4d81-a35c-9b4bbd2bdb3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGirlRU",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b60a2718-02a5-4abe-8225-25d7d2c73f67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2685f5a9-d28d-4d81-a35c-9b4bbd2bdb3c",
            "compositeImage": {
                "id": "214e357c-95c5-4c6d-9af8-771682cde11d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b60a2718-02a5-4abe-8225-25d7d2c73f67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8f73228-09fb-41ae-bc7a-52f57cc15cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b60a2718-02a5-4abe-8225-25d7d2c73f67",
                    "LayerId": "d9ae5fb3-78f0-487b-99cf-5255a6b46f08"
                }
            ]
        },
        {
            "id": "55dff065-83de-4fa5-a905-b32623858449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2685f5a9-d28d-4d81-a35c-9b4bbd2bdb3c",
            "compositeImage": {
                "id": "8601eaa1-4c38-41b8-9b9f-11053421dd02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55dff065-83de-4fa5-a905-b32623858449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5e43c05-1474-405a-b590-9d757400d728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55dff065-83de-4fa5-a905-b32623858449",
                    "LayerId": "d9ae5fb3-78f0-487b-99cf-5255a6b46f08"
                }
            ]
        },
        {
            "id": "0feddbc0-33b0-4120-8d63-8d4e26d8c61e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2685f5a9-d28d-4d81-a35c-9b4bbd2bdb3c",
            "compositeImage": {
                "id": "6626020a-8959-41cb-ab7a-ef9ddcb7893b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0feddbc0-33b0-4120-8d63-8d4e26d8c61e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "649b050b-7cb8-4ef0-a103-3f77a8f5ff57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0feddbc0-33b0-4120-8d63-8d4e26d8c61e",
                    "LayerId": "d9ae5fb3-78f0-487b-99cf-5255a6b46f08"
                }
            ]
        },
        {
            "id": "40ca7fac-5aa2-4bd2-bb28-2b92f81ee5ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2685f5a9-d28d-4d81-a35c-9b4bbd2bdb3c",
            "compositeImage": {
                "id": "755338ce-2448-4f3b-8224-d755da766431",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40ca7fac-5aa2-4bd2-bb28-2b92f81ee5ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3203eef1-f197-43b0-baa3-eb83c7c7388c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40ca7fac-5aa2-4bd2-bb28-2b92f81ee5ea",
                    "LayerId": "d9ae5fb3-78f0-487b-99cf-5255a6b46f08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d9ae5fb3-78f0-487b-99cf-5255a6b46f08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2685f5a9-d28d-4d81-a35c-9b4bbd2bdb3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        553648127,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4280173158,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}