{
    "id": "2e25f7a8-9369-4868-ad1a-7b5afb47cfef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6abb14fb-a1f7-4df8-9638-983039e30e49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e25f7a8-9369-4868-ad1a-7b5afb47cfef",
            "compositeImage": {
                "id": "2b59d9c8-6efd-4d84-8998-a219e936549d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6abb14fb-a1f7-4df8-9638-983039e30e49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a78f0490-73ba-4102-9d3d-cd61d9143b02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6abb14fb-a1f7-4df8-9638-983039e30e49",
                    "LayerId": "9354de62-47c8-43a8-9566-41c337cef0b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9354de62-47c8-43a8-9566-41c337cef0b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e25f7a8-9369-4868-ad1a-7b5afb47cfef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}