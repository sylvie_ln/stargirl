var xp = argument[0];
var yp = argument[1];
var rval = false;
ds_list_clear(actors);
var num = instance_place_list(xp,yp,oActor,actors,true);
for(var i=0; i<num; i++) {
	var actor = actors[|i];
	if object_index == oChar and actor.object_index == oLonck {
		if keys > 0 {
			if actor.unlock == 0 {
				keys--;
				with actor { event_user(0); event_user(eCollect.get); }
			}
		}
	}
	if object_index == oChar and actor.object_index == oDarkStar {
		rval = true;	
	}
	if actor.blocker { 
		if  !(object_index == oStar and actor.object_index == oChar) 
		and !(object_index == oDarkStar and actor.object_index == oTrundler)
		and !(object_index == oDarkStar and actor.object_index == oFinalBoss)
		and !(object_index == oTrundler and actor.object_index == oDarkHole) {
			rval = true; 
		}
	}
	if team != teams.neutral {
		with actor {
			if team == -other.team {
				event_user(eActor.hurt);
			}
		}
	}
	if instance_exists(actor) {
		if actor.team != teams.neutral {
			with actor {
				with other {
					if team == -actor.team {
						event_user(eActor.hurt);	
					}
				}
			}
		}
	}
}
if object_index != oChar {
	if argument[0] < 0 
	or argument[0] >= global.screen_width
	or argument[1] < 0
	or argument[1] >= global.screen_height {
		rval = true;	
	}
}
return rval;