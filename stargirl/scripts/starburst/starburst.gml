///@param x
///@param y
///@param depth
///@param obj
///@param num
///@param angle
///@param speed
///@param time
var xp = argument[0];
var yp = argument[1];
var de = argument[2]
var obj = argument[3];
var num = argument[4];
var angle = argument[5];
var spd = argument[6]
var time = argument[7];
for(var i=angle; i<angle+360; i += (360/num)) {
	var star = instance_create_depth(xp,yp,de,obj);
	if object_is_ancestor(obj,oActor) {
		star.hv = lengthdir_x(spd,i);
		star.vv = lengthdir_y(spd,i);
	} else {
		star.hspeed = lengthdir_x(spd,i);
		star.vspeed = lengthdir_y(spd,i);
	}
	star.time = time;
	with star { event_user(0); }
}