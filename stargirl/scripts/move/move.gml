var amount = argument[0];
var axis = argument[1];
if amount == 0 { return true; }

var target = round_ties_down(amount+subpixel[axis]);
subpixel[axis] += amount-target;

var xa = (axis == 0) ? 1 : 0;
var ya = (axis == 1) ? 1 : 0;
var s = sign(target);

while target != 0 {
	if collision_at(x+xa*s,y+ya*s) {
		break;
	}
	x += xa*s;
	y += ya*s;
	target -= s;
}

if target == 0 {
	return true;
} else {
	subpixel[axis] = 0.5*s;
	return false;		
}