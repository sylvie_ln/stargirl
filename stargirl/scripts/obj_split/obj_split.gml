if image_xscale != 1 or image_yscale != 1 {
	var w = sprite_get_width(sprite_index);
	var h = sprite_get_height(sprite_index);
	for(var i=0; i<image_xscale; i++) {
		for(var j=0; j<image_yscale; j++) {
			instance_create_depth(x+i*w,y+j*h,depth,object_index);
		}
	}
	instance_destroy();
	return true;
}
return false;