///@description Assigns a temporary "waiting for input" state to the given action. Used for button config.
///@param action_name String representing the action name.
///@param input_index* The input to replace with a wait.
var action = argument[0];
var list = oInput.actions[? action];
if argument_count <= 1 {
	ds_list_add(list,list_create(input_kind.waiting));
} else {
	var input_index = argument[1];
	var input = list[|input_index];
	ds_list_clear(input);
	ds_list_add(input,input_kind.waiting);
}