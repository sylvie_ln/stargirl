///@param id
///@param item
var set = argument[0];
var item = argument[1];
var items = set[|0];
var contains = set[|1];
if !ds_map_exists(contains,item) {
	contains[? item] = true;
	ds_list_add(items,item);
	return true;
}
return false;