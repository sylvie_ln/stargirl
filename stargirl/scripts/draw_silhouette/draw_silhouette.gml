///@param x
///@param y
///@param blend
///@param alpha
///@param xscale
///@param yscale
///@param angle
var ib = image_blend;
var ia = image_alpha;
var xs = image_xscale;
var ys = image_yscale;
var an = image_angle;
if argument_count > 2 {
	ib = argument[2];	
}
if argument_count > 3 {
	ia = argument[3];	
}
if argument_count > 5 {
	xs = argument[4];	
	ys = argument[5];	
}
if argument_count > 6 {
	an = argument[6];	
}
draw_surface_ext(
get_silhouette_surface(),
argument[0]-sign(image_xscale)*abs(sprite_xoffset*abs(xs/image_xscale)),
argument[1]-sign(image_yscale)*abs(sprite_yoffset*abs(ys/image_yscale)),
xs,ys,an,ib,ia);