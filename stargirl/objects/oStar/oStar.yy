{
    "id": "3274de69-5d21-425e-849c-b75a079c9c88",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStar",
    "eventList": [
        {
            "id": "24b1a44c-a928-4805-bce1-ccbcad08e5b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3274de69-5d21-425e-849c-b75a079c9c88"
        },
        {
            "id": "d115f593-4093-4c71-b806-a1bfe502fbeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3274de69-5d21-425e-849c-b75a079c9c88"
        },
        {
            "id": "695eb85b-e395-4eaf-8e3f-455cb47b464c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3274de69-5d21-425e-849c-b75a079c9c88"
        },
        {
            "id": "99a4b0bd-0ebd-4aa3-93f3-6d323ebda680",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3274de69-5d21-425e-849c-b75a079c9c88"
        },
        {
            "id": "a63cc6fa-7712-4129-8534-b990248f12a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "3274de69-5d21-425e-849c-b75a079c9c88"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fd02b5f9-3445-47aa-9a36-73e1fe513346",
    "visible": true
}