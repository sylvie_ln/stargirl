{
    "id": "90de0075-fe37-442e-8a09-cf2382d33cb5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDarkStar",
    "eventList": [
        {
            "id": "6e9a3ec6-286b-4881-af73-4418f40c7322",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "90de0075-fe37-442e-8a09-cf2382d33cb5"
        },
        {
            "id": "1bb7f8ab-762c-401e-98d0-9ba28ff73ab4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90de0075-fe37-442e-8a09-cf2382d33cb5"
        }
    ],
    "maskSpriteId": "fd02b5f9-3445-47aa-9a36-73e1fe513346",
    "overriddenProperties": null,
    "parentObjectId": "3274de69-5d21-425e-849c-b75a079c9c88",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2405d297-2427-4ed4-b2a4-05cebfbaea19",
    "visible": true
}