{
    "id": "5b09f8ca-2103-4164-a4c0-987259b73bcb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLonck",
    "eventList": [
        {
            "id": "cc384a25-0080-46cb-91ee-362977be6bff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5b09f8ca-2103-4164-a4c0-987259b73bcb"
        },
        {
            "id": "762cb4d8-3904-464b-ae0e-6e57f5fbc239",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5b09f8ca-2103-4164-a4c0-987259b73bcb"
        },
        {
            "id": "ae9cb0a2-0727-4698-aa39-4fffdbdfb0b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5b09f8ca-2103-4164-a4c0-987259b73bcb"
        },
        {
            "id": "69b2cf88-1112-4dd6-9937-20042fff6748",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "5b09f8ca-2103-4164-a4c0-987259b73bcb"
        },
        {
            "id": "78d0d15a-1dfc-4724-bdb1-d41d1500873b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5b09f8ca-2103-4164-a4c0-987259b73bcb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c282dff2-658a-4b12-9641-e63c01441364",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1631c0c9-6bb9-49d4-b6c9-a84fc2ae3026",
    "visible": true
}