if chase {
	var dir = -1;
	if !oChar.dead {
		if collision_line(x,y,oChar.x,oChar.y+6,oBlock,false,true) == noone {
			dir = point_direction(x,y,oChar.x,oChar.y+6);
		}
	} 
	if dir != -1 {
		hv = lengthdir_x(spd,dir);
		vv = lengthdir_y(spd,dir);
	}
} else {
	if xprevious == x and yprevious == y {
		var guidance = collision_point(x,y,oGuidance,false,true);
	} else {
		var guidance = collision_line(xprevious,yprevious,x,y,oGuidance,false,true);
	}
	if guidance != noone and guidance != last {
		var snap = true;
		switch(guidance.object_index) {
			case oUpArrow:     hv = 0; vv = -spd; break;
			case oDownArrow:   hv = 0; vv = spd;  break;
			case oLeftArrow:   vv = 0; hv = -spd; break;
			case oRightArrow:  vv = 0; hv = spd;  break;
			case oEnemyAction: chase = true; snap = false; break;
		}
		last = guidance;
		if snap {
			x = guidance.x;
			y = guidance.y;
		}
	}
	if shooter and guidance == noone {
		last = noone;	
	}
}

if hv != 0 {
	image_xscale = sign(hv);	
}

if shooter {
	var fdir = image_xscale;
	var xp = x+(12*fdir) + irandom_range(-4,4);
	var yp = y + irandom_range(-1,1);
	var star = instance_create_depth(xp,yp,depth-1,oDarkStar);
	star.hv = fdir*star_spd;
}

if !knocked {
	if !move(hv,0) {
		hv = -hv;
	}
	if !move(vv,1) {
		vv = -vv;
	}
}