{
    "id": "1621d850-825d-477a-893b-b0421fff9b7b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTrundler",
    "eventList": [
        {
            "id": "1da7c797-3b8f-44e1-a7e6-e67bc8eaa7b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1621d850-825d-477a-893b-b0421fff9b7b"
        },
        {
            "id": "df742c56-93f3-41b8-9f8b-0680590298e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1621d850-825d-477a-893b-b0421fff9b7b"
        },
        {
            "id": "3f575318-8076-4262-a7f5-1da105cc0f5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 21,
            "eventtype": 7,
            "m_owner": "1621d850-825d-477a-893b-b0421fff9b7b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f79f0c8-86c0-44a0-a0a2-f03ccf988aab",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fbc0165d-8d48-48af-b44b-8e61eefbb94d",
    "visible": true
}