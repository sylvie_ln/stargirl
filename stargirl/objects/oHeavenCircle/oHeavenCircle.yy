{
    "id": "952ec08c-42f1-4f70-9e58-5a06b83a2155",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHeavenCircle",
    "eventList": [
        {
            "id": "43fd48c0-ea1a-48d9-b54f-ed24b4c7f093",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "952ec08c-42f1-4f70-9e58-5a06b83a2155"
        },
        {
            "id": "d04b5635-dff4-4bdc-b5bc-620d84d80b08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "952ec08c-42f1-4f70-9e58-5a06b83a2155"
        },
        {
            "id": "deee6e4d-b502-400b-a669-e66e7fd0a6e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "952ec08c-42f1-4f70-9e58-5a06b83a2155"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0d492430-4b9d-4ebb-bb78-55d7fc337b82",
    "visible": true
}