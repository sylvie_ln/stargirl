if image_alpha != 1 and !instance_exists(oEnemy) {
	fade = true;
	active = true;
}

if fade {
	image_alpha += 1/room_speed;
	if image_alpha > 1 { 
		image_alpha = 1; 
		fade = false;
	}
}

var name = room_get_name(room);
var zone = string_char_at(name,3);
var num = real(string_char_at(name,4));
if active {
	if collision_circle(x,y,2,oChar,false,true) != noone {
		if zone == "X" {
			if room == rmX0 {
				room_goto(rmJ1);	
			} else if room == rmX1 {
				room_goto(rmT1);	
			}
		} else {
			num += dir;
			if num == 0 {
				if zone == "J" {
					room_goto(rmX0);	
				} else if zone == "T" {
					room_goto(rmX1);	
				}
			} else {
				var rm = asset_get_index("rm"+zone+string(num))
				if room_exists(rm) {
					room_goto(rm);
				}
			}
		}
	}
} else if image_alpha != 0 {
	if !place_meeting(x,y,oChar) {	
		active = true;	
	}
}