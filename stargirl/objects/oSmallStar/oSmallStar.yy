{
    "id": "31a5f065-2406-4bcf-8974-c1888962e4df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSmallStar",
    "eventList": [
        {
            "id": "5f1be89a-0219-4210-af8b-ac49bb2771d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "31a5f065-2406-4bcf-8974-c1888962e4df"
        },
        {
            "id": "4255b4d8-6a86-4f15-b46f-e72c1d4f3012",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "31a5f065-2406-4bcf-8974-c1888962e4df"
        },
        {
            "id": "34f183ad-2e24-4621-97ba-d23e5a9d96de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "31a5f065-2406-4bcf-8974-c1888962e4df"
        },
        {
            "id": "57e53606-b8ba-4324-97fc-616bcf2ece9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "31a5f065-2406-4bcf-8974-c1888962e4df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "776ad9ae-06d8-48a6-9127-352e486a0921",
    "visible": true
}