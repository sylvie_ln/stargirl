if placed { exit; }
x += hv;
y += vv;
if chase {
	var dir = -1;
	if !oChar.dead {
		dir = point_direction(x,y,oChar.x,oChar.y);
	}
	if dir != -1 {
		hv = lengthdir_x(spd,dir);
		vv = lengthdir_y(spd,dir);
	}
} else {
	hv *= fric;
	vv *= fric;
	if abs(hv) < 0.5 { chase = true; }
	if abs(vv) < 0.5 { chase = true; }
}