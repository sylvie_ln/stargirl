{
    "id": "fa8d9b15-9a53-419e-a5ac-18156d7fe71c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGem",
    "eventList": [
        {
            "id": "cd8162fd-ca01-4d1b-a4af-6866e20eaac0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "fa8d9b15-9a53-419e-a5ac-18156d7fe71c"
        },
        {
            "id": "f6e96b6d-13fc-407e-a27b-6e9bc174f717",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fa8d9b15-9a53-419e-a5ac-18156d7fe71c"
        },
        {
            "id": "de23ecd0-868d-4882-b05e-284cbe9747d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fa8d9b15-9a53-419e-a5ac-18156d7fe71c"
        },
        {
            "id": "965ea9e2-536c-43c7-8c7c-7cf1eafc77e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "fa8d9b15-9a53-419e-a5ac-18156d7fe71c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c282dff2-658a-4b12-9641-e63c01441364",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2e25f7a8-9369-4868-ad1a-7b5afb47cfef",
    "visible": true
}