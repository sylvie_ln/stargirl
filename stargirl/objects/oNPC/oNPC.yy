{
    "id": "8cc2a363-b7fe-4d69-8a3d-e0fd673f217b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNPC",
    "eventList": [
        {
            "id": "6489be0a-2017-4bc5-8727-ff40468fc546",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8cc2a363-b7fe-4d69-8a3d-e0fd673f217b"
        },
        {
            "id": "0514efe3-ffad-4466-b7e9-8cdd0c9155f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8cc2a363-b7fe-4d69-8a3d-e0fd673f217b"
        },
        {
            "id": "be4680a1-f08e-4b1b-b3dd-0ca071461526",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 18,
            "eventtype": 7,
            "m_owner": "8cc2a363-b7fe-4d69-8a3d-e0fd673f217b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "654f966b-a719-48ca-af15-2c69e9e93353",
    "visible": true
}