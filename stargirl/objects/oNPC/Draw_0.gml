draw_self();
if text != "" {
	draw_set_font(global.sylvie_font_bold);
	draw_set_color(c_white);
	if room == rmSEnd {
		draw_set_color(c_black);	
	}
	draw_text_centered(x,bbox_bottom,text);
}