var xp = 
round(x+random_range(-shake,shake));
var yp =
round(y+random_range(-shake,shake));
var xs =
image_xscale*scale;
var ys = 
image_yscale*scale;
draw_sprite_ext(
sprite_index,
image_index,
xp,yp,
xs,ys,
image_angle,
image_blend,
image_alpha);
if flash mod 2 == 1 {
	draw_silhouette(xp,yp,(flash mod 4 == 1) ? flash_color : c_white,image_alpha,xs,ys,image_angle)
}
if flash2 mod 2 == 1 {
	draw_silhouette(xp,yp,(flash2 mod 4 == 1) ? c_fuchsia : c_white,image_alpha,xs,ys,image_angle)
}