event_inherited();
subpixel = [0,0];
blocker = false;
actors = ds_list_create();
hv = 0;
vv = 0;


team = teams.neutral;
hp = 0;
damage = 0;
dead = false;
invincible = false;
knockable = false;
knocked = false;
knockback_dir = 0;
knockback_spd = 0;
knockback_force = 8;


shake = 0;
flash = 0;
flash2 = 0;
flash_time = room_speed div 2;
flash_color = c_blue;
scale = 1;

enum teams {
	neutral = 0,
	player = 1,
	enemy = -1
}

enum eActor {
	hurt = 10,
	hurt_special = 11,
}