if scale > 1 {
	scale *= 0.9;
	if scale <= 1 {
		scale = 1;	
	}
}
if shake > 0 {
	shake *= 0.9;
	if shake < 0.5 {
		shake= 0;	
	}
}
if flash > 0 {
	flash--;
	if flash <= 0 {
		flash = 0;	
	}
}	
if flash2 > 0 {
	flash2--;
	if flash2 <= 0 {
		flash2 = 0;	
	}
}	

knocked = knockback_spd > 0 and knockback_dir != -1;
if knocked {
	var khv = lengthdir_x(knockback_spd,knockback_dir);
	var kvv = lengthdir_y(knockback_spd,knockback_dir);
	if !move(khv,0) {
		//knockback_dir = point_direction(0,0,-khv,kvv);	
	}
	if !move(kvv,1) {
		//knockback_dir = point_direction(0,0,khv,-kvv);	
	}
	knockback_spd *= 0.75;
	if knockback_spd < 0.5 {
		knockback_spd = 0;	
		knockback_dir = -1;
	}	
}