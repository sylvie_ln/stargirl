{
    "id": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oActor",
    "eventList": [
        {
            "id": "ceeae8d4-c8df-46ce-841c-107d6eda5f31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "582d1e77-09b1-49ee-b6bd-c4f0268d89fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "0f9e77ad-d669-432b-8bf3-f0040b2b90f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 20,
            "eventtype": 7,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "c96eb6d5-4885-4003-ab62-98d5a507335b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "034cd44c-f537-47b8-96a6-e873f207db78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}