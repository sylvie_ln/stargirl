if invincible { exit; }
if other.damage == 0 { exit; }
if other.dead { exit; }

if flash == 0 {
	event_user(eActor.hurt_special);
	if hp > 0 {
		hp -= 1;
		if hp <= 0 {
			dead = true;
		}
	}
	scale = 2;
	flash = flash_time
	if knockable {
		if team == teams.enemy {
			knockback_dir = point_direction(oChar.x,oChar.y,x,y);
		} else {
			knockback_dir = point_direction(other.x,other.y,x,y);
		}
		knockback_spd = other.knockback_force;
	}
}