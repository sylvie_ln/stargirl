starburst(x,y,depth-1,oDarkSmallStar,8,0,16,room_speed div 10);
starburst(x,y,depth-1,oDarkSmallStar,16,0,12,room_speed div 5);
starburst(x,y,depth-1,oGem,gems,
instance_exists(oChar) ? point_direction(oChar.x,oChar.y,x,y) : choose(90,270),
16,-1);