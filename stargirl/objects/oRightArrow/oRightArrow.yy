{
    "id": "205eff3f-f9d2-4e39-8cc2-8eef27db5427",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRightArrow",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "60969700-8019-4a31-abf3-5234aec20c40",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "baac9249-1aa7-4b67-9474-bccdccc889c8",
    "visible": true
}