draw_set_color(c_black);
draw_rectangle(0,global.screen_height,global.screen_width,global.view_height,false);
draw_set_font(global.sylvie_font_bold);
draw_set_color(c_white);
var b = 2;
var hpos = 2*b;
var vpos = global.screen_height+b;
hpos += 8;
vpos += 8;
draw_sprite(sKey,0,hpos,vpos);
hpos += 8;
vpos -= 8;
t = ((keys <= 9) ? "0" : "")+string(keys);
draw_text(hpos,vpos,t);
hpos += 20;
vpos += 8;
draw_sprite(sGem,0,hpos,vpos);
hpos += 8;
vpos -= 8;
t = ((gems <= 99) ? "0" : "")+((gems <= 9) ? "0" : "")+string(gems);
draw_text(hpos,vpos,t);
hpos += 28;
vpos += 8;
for(var i=0; i<hp-1; i++) {
	draw_sprite(sHeart,0,hpos,vpos);
	hpos += 16;
}
vpos -= 8;
hpos = global.screen_width-2*b;
t = string_upper(oPrincess.area_name)
draw_set_halign(fa_right);
draw_text(hpos,vpos,t);
draw_set_halign(fa_left);