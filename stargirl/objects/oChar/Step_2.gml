if dead {
	if alarm[0] < 0 {
		starburst(x,y,depth-1,oSmallStar,8,0,6,-1);
		starburst(x,y,depth-1,oSmallStar,8,22.5,8,-1);
		starburst(x,y,depth-1,oSmallStar,16,0,12,-1);
		starburst(x,y,depth-1,oSmallStar,16,11.25,16,-1);
		alarm[0] = room_speed*3;
	}	
	exit;
}

if x < 0 {
	x += global.screen_width;
	room_shift(edir.left);	
} else if x >= global.screen_width {
	x -= global.screen_width;
	room_shift(edir.right);	
} else if bbox_bottom-3 < 0 {
	y += global.screen_height;
	room_shift(edir.up);	
} else if bbox_bottom-3 >= global.screen_height {
	y -= global.screen_height;
	room_shift(edir.down);	
}

