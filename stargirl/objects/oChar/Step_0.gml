if dead { exit; }

with instance_place(x,y,oCollect) {
	if !visible { continue; }
	if price > other.gems {
		continue;	
	}
	other.gems -= price;
	event_user(eCollect.get);
	if room != rmT6 or global.hell {
		instance_destroy();	
	}
}

if room == rmJ6 and place_meeting(x,y,oNPC) {
	dead = true;
	hp = 0;
	exit;
}

if global.hell and room == rmT6 and place_meeting(x,y,oNPC) {
	with oNPC {
		event_user(8);	
	}
	exit;
}

var up = input_held("Up");
var down = input_held("Down");
var left = input_held("Left");
var right = input_held("Right");
var hdir = right-left;
if left and right {
	hdir = input_held_time("Left") < input_held_time("Right") ? -1 : 1;	
}
var vdir = down-up;
if up and down {
	vdir = input_held_time("Up") < input_held_time("Down") ? -1 : 1;	
}

hv = 0;
vv = 0;
if hdir != 0 or vdir != 0 {
	var dir = point_direction(0,0,hdir,vdir);
	hv = lengthdir_x(spd,dir);
	vv = lengthdir_y(spd,dir);
}

if !knocked {
	if !move(hv,0) { 
		hv = 0;
	}
	if !move(vv,1) {
		vv = 0;
	}
}

if input_pressed("A2") {
	fdir = -fdir;	
}
var shoot = input_held("A1");
if shoot and (input_held_time("A1") mod rate == 1 mod rate) {
	var xp = x + irandom_range(-1,1);
	var yp = y+(12*fdir) + irandom_range(-4,4);
	var star = instance_create_depth(xp,yp,depth-fdir,oStar);
	star.vv = fdir*star_spd;
	if input_pressed("A1") {
		starburst(x,y,depth-fdir,oSmallStar,8,22.5,8,room_speed div 10);
	}
}

var spr = "sGirl";
if hdir != 0 or vdir != 0 or shoot {
	spr += "R"	
} else {
	spr += "S";	
}
if vdir != 0 and !shoot {
	if input_pressed("Up") or input_pressed("Down") {
		fdir = vdir;
	}
}
if fdir == 1 {
	spr += "D";
} else {
	spr += "U";	
}
spr = asset_get_index(spr);
if sprite_exists(spr) {
	sprite_index = spr;	
}
