{
    "id": "b1ca0b92-4b92-4359-9f02-3bc0ef401e92",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChar",
    "eventList": [
        {
            "id": "6a7c73da-8788-4414-a1c4-ff9dbe5c4479",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b1ca0b92-4b92-4359-9f02-3bc0ef401e92"
        },
        {
            "id": "929899fd-d988-4f04-910c-d95d551f06a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b1ca0b92-4b92-4359-9f02-3bc0ef401e92"
        },
        {
            "id": "ca94e6b9-47c0-491d-af62-dcf206b631ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "b1ca0b92-4b92-4359-9f02-3bc0ef401e92"
        },
        {
            "id": "6d62025e-8294-4322-a697-6acb168c46e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b1ca0b92-4b92-4359-9f02-3bc0ef401e92"
        },
        {
            "id": "f04bd642-5f16-4cbd-8188-0c84399da30b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "b1ca0b92-4b92-4359-9f02-3bc0ef401e92"
        },
        {
            "id": "58c382ee-5d45-4d1c-9aac-70c524181ca9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b1ca0b92-4b92-4359-9f02-3bc0ef401e92"
        }
    ],
    "maskSpriteId": "c9795d6b-f211-4752-aa0b-25ebee6200c2",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4f99db57-1fdd-4361-a24f-21c479af1763",
    "visible": true
}