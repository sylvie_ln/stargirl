placed = true;
if ds_map_exists(global.collected,key) and global.collected[? key] {
	with instance_place(x,y,oMagicCircle) {
		instance_destroy();
	}
	instance_destroy();	
}
if place_meeting(x,y,oMagicCircle) {
	visible = !instance_exists(oEnemy);
}