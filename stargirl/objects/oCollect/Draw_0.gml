event_inherited();

if price != 0 {
	draw_set_font(global.sylvie_font_bold);
	draw_set_color(c_white);
	draw_text_centered(x,bbox_bottom+2,string(price));
}