event_inherited();
depth = 20;
key = room_get_name(room)+"_"+object_get_name(object_index)+"_";
if ds_map_exists(global.collectable_id,key) {
	global.collectable_id[? key]++;
} else {
	global.collectable_id[? key] = 0;
}
num = string(global.collectable_id[? key]);
key += num;
placed = false;

enum eCollect {
	get = 4	
}

price = 0;