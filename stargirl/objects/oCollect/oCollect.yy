{
    "id": "c282dff2-658a-4b12-9641-e63c01441364",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCollect",
    "eventList": [
        {
            "id": "50f18a8f-2bbe-4284-a1b1-793bbd51c51d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c282dff2-658a-4b12-9641-e63c01441364"
        },
        {
            "id": "170d3cb2-1fcc-469a-9ee0-ea77d5a7d32d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "c282dff2-658a-4b12-9641-e63c01441364"
        },
        {
            "id": "e90d80de-bcb9-4fdd-973a-eb0aef39acba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "c282dff2-658a-4b12-9641-e63c01441364"
        },
        {
            "id": "4e344c71-bd8b-43b6-9853-51f655950d09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "c282dff2-658a-4b12-9641-e63c01441364"
        },
        {
            "id": "ea9336a1-e786-4e21-8fe7-bfce0a9add60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "c282dff2-658a-4b12-9641-e63c01441364"
        },
        {
            "id": "3ed1341e-d9ea-4cb7-bd96-a6bd14de836a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c282dff2-658a-4b12-9641-e63c01441364"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}