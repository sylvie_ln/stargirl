{
    "id": "b245900b-55ca-4087-b3a3-156ac08bbc36",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLeftArrow",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "60969700-8019-4a31-abf3-5234aec20c40",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dd2d3089-6c3d-4a00-b823-8fc882874b96",
    "visible": true
}