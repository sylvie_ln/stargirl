{
    "id": "fd6e8eae-bf90-40da-9468-21a01f08465a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDarkSmallStar",
    "eventList": [
        {
            "id": "9da12013-f849-4b0d-817f-9b750ac41e12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd6e8eae-bf90-40da-9468-21a01f08465a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "31a5f065-2406-4bcf-8974-c1888962e4df",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2ca4657b-6830-4b63-b5ad-ddbbcb3dc140",
    "visible": true
}