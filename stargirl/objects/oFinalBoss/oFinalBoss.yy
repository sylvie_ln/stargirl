{
    "id": "fc6ba903-2564-4487-9a20-a3e1ad990ad0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFinalBoss",
    "eventList": [
        {
            "id": "b468a490-3bae-40d6-ab6f-3575d18382fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fc6ba903-2564-4487-9a20-a3e1ad990ad0"
        },
        {
            "id": "d8e43a4b-6e27-402a-8b93-7a83001ee607",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fc6ba903-2564-4487-9a20-a3e1ad990ad0"
        },
        {
            "id": "db6f0715-d63e-4a16-97ca-fbe1e9c94ac2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "fc6ba903-2564-4487-9a20-a3e1ad990ad0"
        },
        {
            "id": "f269c437-aeb2-4b3a-a9a3-eb7da405e50e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "fc6ba903-2564-4487-9a20-a3e1ad990ad0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f79f0c8-86c0-44a0-a0a2-f03ccf988aab",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "654f966b-a719-48ca-af15-2c69e9e93353",
    "visible": true
}