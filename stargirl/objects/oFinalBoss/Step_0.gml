if !move(hv,0) {
	hv = -hv;
}
if !move(vv,1) {
	vv = -vv;
}
if hp <= 8 {
	if scale == 1 and stepc mod ceil(lerp(room_speed,1,(8-hp)/7)) == 0 {
		scale = 1.5;	
		flash2 = 5;	
	} else {
		stepc++;	
	}
}

dir = point_direction(0,0,hv,vv);
spd = lerp(2,4,(max_hp-hp)/(max_hp-1));
hv = lengthdir_x(spd,dir);
vv = lengthdir_y(spd,dir);