{
    "id": "97d5e328-ac83-453f-a034-8788b0f2963b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLargeGem",
    "eventList": [
        {
            "id": "ee778a25-d614-4bb4-b96a-5c4286394bbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "97d5e328-ac83-453f-a034-8788b0f2963b"
        },
        {
            "id": "73042f27-bf87-412d-a897-385d7e192661",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97d5e328-ac83-453f-a034-8788b0f2963b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c282dff2-658a-4b12-9641-e63c01441364",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cd5f267c-c6bc-499f-857b-baf0c122ba6c",
    "visible": true
}