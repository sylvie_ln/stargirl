{
    "id": "91b921e2-95bd-4d5b-b390-e07bd11e608e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHeart",
    "eventList": [
        {
            "id": "4f11469d-b0e2-4875-b1ec-c36081716458",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "91b921e2-95bd-4d5b-b390-e07bd11e608e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c282dff2-658a-4b12-9641-e63c01441364",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e93f7a5a-ab21-428b-a557-9699829a80bb",
    "visible": true
}