{
    "id": "1b62d6f9-b5c8-4b46-bacb-1aea5d9bbc2f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDownArrow",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "60969700-8019-4a31-abf3-5234aec20c40",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cda3617a-4681-466f-9159-bf6f77e59147",
    "visible": true
}