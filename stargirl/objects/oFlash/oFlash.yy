{
    "id": "0fd0c9c3-cdd1-47ed-8f6d-8cdb6b6b6102",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFlash",
    "eventList": [
        {
            "id": "9d188738-fe0c-4073-b975-f959422036f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0fd0c9c3-cdd1-47ed-8f6d-8cdb6b6b6102"
        },
        {
            "id": "ccf9c27b-f8cc-42cf-8f87-0fac7e3d1e31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "0fd0c9c3-cdd1-47ed-8f6d-8cdb6b6b6102"
        },
        {
            "id": "41a88788-3201-4f70-9b48-89f1ea0c4640",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0fd0c9c3-cdd1-47ed-8f6d-8cdb6b6b6102"
        },
        {
            "id": "7dbf88aa-a9c1-49e9-825a-e3226f8ae015",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "0fd0c9c3-cdd1-47ed-8f6d-8cdb6b6b6102"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}