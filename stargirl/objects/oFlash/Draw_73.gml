var cx = camera_get_view_x(view_camera[0]);
var cy = camera_get_view_y(view_camera[0]);
var cw = global.view_width;
var ch = global.view_height;
draw_set_color(c_white);
draw_set_alpha(time/total);
draw_rectangle(cx,cy,cx+cw,cy+ch,false);
draw_set_alpha(1);