{
    "id": "95bb9363-7899-470e-8273-5b454d77576a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oUpArrow",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "60969700-8019-4a31-abf3-5234aec20c40",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bbea60b9-1afe-4e81-951f-2ad00fc4b430",
    "visible": true
}