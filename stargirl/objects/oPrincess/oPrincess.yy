{
    "id": "557710c1-6855-495d-b33f-a4e85b05ac13",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPrincess",
    "eventList": [
        {
            "id": "d6f3282e-6634-4bb3-973f-1e006099761f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "557710c1-6855-495d-b33f-a4e85b05ac13"
        },
        {
            "id": "9d857f3b-8cc3-4e03-bc90-7a203f2f1d30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "557710c1-6855-495d-b33f-a4e85b05ac13"
        },
        {
            "id": "7f4ba157-6590-4624-a278-c61c4806c2f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "557710c1-6855-495d-b33f-a4e85b05ac13"
        },
        {
            "id": "c86fcf11-eaef-464a-bb82-8c957851d850",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "557710c1-6855-495d-b33f-a4e85b05ac13"
        },
        {
            "id": "445db690-23a1-4e4a-b9fe-19284837a2b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "557710c1-6855-495d-b33f-a4e85b05ac13"
        },
        {
            "id": "eba747cc-b783-4a89-bd0e-65ffcb4d2f02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "557710c1-6855-495d-b33f-a4e85b05ac13"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}