var name = room_get_name(room);
if string_length(name) != 4 { exit; }

ds_map_clear(global.collectable_id);
global.darkhole = 0;

layer_set_visible("Grid",false);
var bg = layer_background_get_id("Background");
var zone = string_char_at(name,3);
var num = real(string_char_at(name,4));
if zone == "A" {
	layer_background_blend(bg,make_color_hsl_240(80,240,20));	
	area_name = "Stella"
}
if zone == "B" {
	layer_background_blend(bg,make_color_hsl_240(20,240,20));	
	area_name = "Sophie"
}
if zone == "C" {
	layer_background_blend(bg,make_color_hsl_240(40,240,20));	
	area_name = "Serene"
}
if zone == "D" {
	layer_background_blend(bg,make_color_hsl_240(180,240,20));	
	area_name = "Sylvia"
}
area_name += " "
switch(num) {
	case 1: area_name += "North"; break;
	case 2: area_name += "East"; break;
	case 3: area_name += "South"; break;
	case 4: area_name += "West"; break;
}
if zone == "X" {
	layer_background_blend(bg,make_color_hsl_240(0,0,20));	
	area_name = "Entrance";
}
if zone == "J" {
	layer_background_blend(bg,make_color_hsl_240(0,240,20));	
	area_name = "Hell's Descent";
	if num == 6 {
		area_name = "Hell"	
		global.hell = true;
	}
}
if zone == "T" {
	layer_background_blend(bg,make_color_hsl_240(140,240,40));	
	area_name = "Heaven's Tower";
	if num == 6 {
		area_name = "Heaven";	
	}
}