alarm[0] = room_speed;
if instance_number(oDarkHole) > 1 and hell {
	var friend = noone;
	while friend == noone {
		friend = instance_find(oDarkHole,irandom(instance_number(oDarkHole)-1));
		if friend == self { friend = noone; }
	}
	with friend {
		event_perform(ev_alarm,0);
		break
	}
}