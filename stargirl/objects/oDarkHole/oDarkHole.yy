{
    "id": "7e356fd6-54e1-4128-99af-3b966d598e84",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDarkHole",
    "eventList": [
        {
            "id": "d5d54cc1-ebab-44ed-ab39-9c0419856364",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7e356fd6-54e1-4128-99af-3b966d598e84"
        },
        {
            "id": "1de9a229-fb91-4ad3-aef9-76c4743ee42f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7e356fd6-54e1-4128-99af-3b966d598e84"
        },
        {
            "id": "e47be03c-b8ca-43f2-bcc7-d14b6de4393b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 21,
            "eventtype": 7,
            "m_owner": "7e356fd6-54e1-4128-99af-3b966d598e84"
        },
        {
            "id": "b5b3d290-3baf-4893-9e05-ac551a00577d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "7e356fd6-54e1-4128-99af-3b966d598e84"
        },
        {
            "id": "7dceaa78-8965-49f5-9749-fb51a2b83d59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7e356fd6-54e1-4128-99af-3b966d598e84"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f79f0c8-86c0-44a0-a0a2-f03ccf988aab",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a681887-967e-451e-99b4-c3807f470044",
    "visible": true
}