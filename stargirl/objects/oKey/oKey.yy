{
    "id": "6b5f5ceb-6b49-4cbc-92ca-4bf81b506753",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oKey",
    "eventList": [
        {
            "id": "4dd5f878-a4f1-425b-a047-f7f4d3ca64cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "6b5f5ceb-6b49-4cbc-92ca-4bf81b506753"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c282dff2-658a-4b12-9641-e63c01441364",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "83277d49-251e-446f-bec3-39bca5aeffd1",
    "visible": true
}