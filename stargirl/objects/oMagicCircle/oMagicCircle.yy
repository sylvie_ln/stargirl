{
    "id": "207d29e0-4669-480d-9444-ee66d9527c7d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMagicCircle",
    "eventList": [
        {
            "id": "16b3cea0-cfa5-42b6-a5f1-33b6fac5b734",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "207d29e0-4669-480d-9444-ee66d9527c7d"
        },
        {
            "id": "0e76db89-decb-499f-b02f-2d369b8b911f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "207d29e0-4669-480d-9444-ee66d9527c7d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "66c4834c-498d-4ff1-b35c-a5ebff3ec0be",
    "visible": true
}